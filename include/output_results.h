//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a function which can be used to print statistical info. about a set of measurements.

#ifndef TIXL_OUTPUT_RESULTS_H
#define TIXL_OUTPUT_RESULTS_H

#include "perf_stats.h"

#include <utility> // std::size_t
#include <string> // std::string

namespace tixl
{

// function to output the results of 'perform_experiments' and 'mpi_perform_experiments'
void output_results(const std::string& header, const perf_stats& results, const std::size_t& fp_count = 0,
    const std::size_t& data_traffic = 0);

} // namespace tixl
#endif
