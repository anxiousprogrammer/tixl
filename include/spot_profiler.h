//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a class which can be used to perform spot-based profiling.

#ifndef TIXL_SPOT_PROFILER_H
#define TIXL_SPOT_PROFILER_H

#include "perf_stats.h"

#include <string> // std::string
#include <map> // std::map
#include <vector> // std::vector
#include <stdexcept> // std::runtime_error
#include <cstdint> // std::uint64_t

namespace tixl
{

// forward declarations
namespace impl
{
    class spot_profiler_impl;
} // namespace impl

//+//////////////////////////////////////////////
// spot profiler for shared-memory programs
//+//////////////////////////////////////////////

class spot_profiler
{
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    spot_profiler();
    
    spot_profiler(const spot_profiler&) = default;
    
    spot_profiler(spot_profiler&&) = default;
    
    spot_profiler& operator=(const spot_profiler&) = default;
    
    spot_profiler& operator=(spot_profiler&&) = default;
    
    ~spot_profiler();
    
public:
    
    //+/////////////////
    // access
    //+/////////////////
    
    static inline spot_profiler& get()
    {
        static auto singleton = spot_profiler();
        return singleton;
    }
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void open_window();
    
    void open_spot(const std::string& key);
    
    void close_spot(const std::string& key);
    
    void close_window();
    
    void output_results(const std::string& header) const;
    
    //+/////////////////
    // access
    //+/////////////////
    
    enum e_window_stage { unopened, opened, closed };
    
    e_window_stage get_window_stage() const;
    
    perf_stats get_stats(const std::string& key) const;
    
private:
    
    //+/////////////////
    // implementation
    //+/////////////////
    
    void compute_results();
    
    //+/////////////////
    // access (safety measure)
    //+/////////////////
    
    impl::spot_profiler_impl* get_impl()
    {
        if (impl_ == nullptr)
        {
            throw std::runtime_error("spot_profiler_get_impl_nullptr_error");
        }
        return impl_;
    }
    
    const impl::spot_profiler_impl* get_impl() const
    {
        return const_cast<spot_profiler*>(this)->get_impl();
    }
    
    //+/////////////////
    // members
    //+/////////////////
    
    impl::spot_profiler_impl* impl_ = nullptr;
    
    struct results_pack
    {
        std::vector<std::uint64_t> times = {};
        perf_stats stats = {};
    };
    std::map<std::string, results_pack> results_ = {};
};

} // namespace tixl
#endif // TIXL_SPOT_PROFILER_H
