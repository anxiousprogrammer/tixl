//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the functor class which serves as the primary input parameter of the core functions of the TiXL
/// library.

#ifndef TIXL_EXPERIMENT_FUNCTOR_H
#define TIXL_EXPERIMENT_FUNCTOR_H

namespace tixl
{

//+//////////////////////////////////////////////
// a virtual class which is used to create
// functors to pass the code to be tested to the
// functions 'perform_experiments' and
// 'mpi_perform_experiments'
//+//////////////////////////////////////////////

class experiment_functor
{
public:
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual void init() = 0;
    
    virtual void perform_experiment() = 0;
    
    virtual void finish() = 0;
};

} // namespace tixl
#endif
