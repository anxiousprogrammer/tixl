//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a struct which is used to return statistical information.

#ifndef TIXL_PERF_STATS_H
#define TIXL_PERF_STATS_H

#include <utility> // std::size_t
#include <limits> // std::numeric_limits

namespace tixl
{

//+//////////////////////////////////////////////
// a struct which can be used to record/transport
// statistical information about timed
// experiments
//+//////////////////////////////////////////////

struct perf_stats
{
    std::size_t exp_count = 0;
    
    double am_duration = 0.0;  // arithmetic mean
    double gm_duration = 0.0;  // geometric mean
    
    double min_duration = 0.0;
    double max_duration = 0.0;
    
    double std_dev_duration = 0.0;
    double std_dev_inv_duration = 0.0;
};

} // namespace tixl
#endif
