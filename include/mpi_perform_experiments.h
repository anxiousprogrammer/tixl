//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a function to microbenchmark an arbitrary target function using MPI.

#ifndef TIXL_MPI_PERFORM_EXPERIMENTS_H
#define TIXL_MPI_PERFORM_EXPERIMENTS_H

#include "experiment_functor.h"

#include <vector> // std::vector
#include <cstdint> // std::uint64_t

#include <mpi.h>

namespace tixl
{

//+//////////////////////////////////////////////
// function which performs experiments in a
// distributed-memory setup using MPI
//+//////////////////////////////////////////////

std::vector<std::uint64_t> mpi_perform_experiments(MPI_Comm comm, experiment_functor& exp_fn, const std::size_t& exp_count);

} // namespace tixl
#endif
