//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a class which can be used to perform spot-based profiling of MPI-based programs.

#ifndef TIXL_MPI_SPOT_PROFILER_H
#define TIXL_MPI_SPOT_PROFILER_H

#include "perf_stats.h"

#include <string> // std::string
#include <map> // std::map
#include <vector> // std::vector
#include <stdexcept> // std::runtime_error
#include <cstdint> // std::uint64_t

#include <mpi.h>

namespace tixl
{

// forward declarations
namespace impl
{
    class spot_profiler_impl;
} // namespace impl

//+//////////////////////////////////////////////
// spot profiler for distributed-memory programs
// using MPI
//+//////////////////////////////////////////////

class mpi_spot_profiler
{
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    mpi_spot_profiler();
    
    mpi_spot_profiler(const mpi_spot_profiler&) = default;
    
    mpi_spot_profiler(mpi_spot_profiler&&) = default;
    
    mpi_spot_profiler& operator=(const mpi_spot_profiler&) = default;
    
    mpi_spot_profiler& operator=(mpi_spot_profiler&&) = default;
    
    ~mpi_spot_profiler();
    
public:
    
    //+/////////////////
    // access
    //+/////////////////
    
    static inline mpi_spot_profiler& get()
    {
        static auto singleton = mpi_spot_profiler();
        return singleton;
    }
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void open_window();
    
    void open_spot(const std::string& key);
    
    void close_spot(const std::string& key);
    
    void declare_unreachable_spot(const std::string& key);
    
    void close_window(MPI_Comm comm);
    
    void output_results(const std::string& header) const;
    
    void write_results(const std::string& file_name_prefix) const;
    
    //+/////////////////
    // access
    //+/////////////////
    
    enum e_window_stage { unopened, opened, closed };
    
    e_window_stage get_window_stage() const;
    
    inline bool has_results() const
    {
        return !results_.empty();
    }
    
    // NOTE: only relevant on root
    perf_stats get_stats(const std::string& key, const unsigned rank) const;
    
    // NOTE: only relevant on root
    perf_stats get_all_ranks_stats(const std::string& key) const;
    
private:
    
    //+/////////////////
    // implementation
    //+/////////////////
    
    void compute_results(MPI_Comm comm);
    
    //+/////////////////
    // access (safety measure)
    //+/////////////////
    
    impl::spot_profiler_impl* get_impl()
    {
        if (impl_ == nullptr)
        {
            throw std::runtime_error("mpi_spot_profiler_get_impl_nullptr_error");
        }
        return impl_;
    }
    
    const impl::spot_profiler_impl* get_impl() const
    {
        return const_cast<mpi_spot_profiler*>(this)->get_impl();
    }
    
    //+/////////////////
    // members
    //+/////////////////
    
    impl::spot_profiler_impl* impl_ = nullptr;
    
    //
    // NOTE: following only relevant on root
    //
    
    struct results_pack
    {
        std::vector<std::uint64_t> gathered_times = {};
        std::vector<int> gathered_times_counts = {};
        std::vector<int> gathered_times_offsets = {};
        std::vector<perf_stats> stats = {};
        perf_stats all_ranks_stats = {};
    };
    std::map<std::string, results_pack> results_ = {};
    
    std::uint64_t reduced_total_time_ = {};
};

} // namespace tixl
#endif // TIXL_MPI_SPOT_PROFILER_H
