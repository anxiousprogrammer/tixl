//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation(s) of 'spot_profiler.h"

#include "spot_profiler.h"

#include "impl/spot_profiler_impl.h"
#include "compute_statistics.h"

#include <stdexcept> // std::runtime_error
#include <chrono> // std::system_clock
#include <ctime> // std::strftime
#include <cstdio> // std::printf
#include <vector> // std::vector
#include <algorithm> // std::sort
#include <cstdint> // std::uint64_t

namespace tixl
{

//+//////////////////////////////////////////////
// implementation of spot_profiler -> lifecycle
//+//////////////////////////////////////////////

spot_profiler::spot_profiler()
{
    impl_ = new impl::spot_profiler_impl;
}

spot_profiler::~spot_profiler()
{
    if (impl_ != nullptr)
    {
        delete impl_;
        impl_ = nullptr;
    }
}

//+//////////////////////////////////////////////
// implementation of spot_profiler -> main functionality
//+//////////////////////////////////////////////

void spot_profiler::open_window()
{
    get_impl()->open_window();
}

spot_profiler::e_window_stage spot_profiler::get_window_stage() const
{
    switch(get_impl()->get_window_stage())
    {
        case impl::spot_profiler_impl::unopened:
        {
            return unopened;
        }
        case impl::spot_profiler_impl::opened:
        {
            return opened;
        }
        default:
        {
            break;
        }
    };
    
    return closed;
}

void spot_profiler::open_spot(const std::string& key)
{
    get_impl()->open_spot(key);
}

void spot_profiler::close_spot(const std::string& key)
{
    get_impl()->close_spot(key);
}

void spot_profiler::close_window()
{
    // close the window
    get_impl()->close_window();
    
    // compute results
    compute_results();
}

void spot_profiler::output_results(const std::string& header) const
{
    // pre-conditions
    if (results_.empty())
    {
        throw std::runtime_error("tixl_spot_profiler_output_results_results_unavailable_error");
    }
    
    // header
    const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    auto time_stamp_buffer = std::string(30, '\0');
    std::strftime(time_stamp_buffer.data(), time_stamp_buffer.size(), "%d.%m.%Y|%H:%M:%S", std::localtime(&time_stamp));
    std::printf("\n+=================================================\n"
                  " Spot Profiler Output\n"
                  " Time stamp: %s\n"
                  " %s\n"
                  "+=================================================\n",
        time_stamp_buffer.c_str(),
        header.c_str());
    
    // quick exit
    if (results_.empty())
    {
        std::printf("    No spots available.\n"
                    "\n+=================================================\n");
        return;
    }
    
    // compute percentages
    using results_iter_t = std::remove_const<std::remove_reference<decltype(results_)>::type>::type::const_iterator;
    auto percentages = std::vector<std::pair<results_iter_t, double>>();
    percentages.reserve(results_.size());
    const auto total_time_window = static_cast<double>(get_impl()->total_time);
    for (auto iter_results = results_.cbegin(); iter_results != results_.cend(); ++iter_results)
    {
        auto total_time_spot = std::uint64_t{};
        for (const auto time : iter_results->second.times)
        {
            total_time_spot += time;
        }
        const auto percentage = static_cast<double>(total_time_spot) / total_time_window * 100.0;
        if (percentage > 100.0 + 1E-3)
        {
            throw std::logic_error("tixl_spot_profiler_output_results_percentage_greater_than_100_error");
        }
        percentages.emplace_back(std::make_pair(iter_results, percentage));
    }
    
    // sort them
    std::sort(percentages.begin(), percentages.end(),
        [](const auto& v1, const auto& v2)
        {
            return v1.second > v2.second;
        });
    
    // output statistics
    for (const auto& percentage : percentages)
    {
        const auto& results_element = *percentage.first;
        const auto& stats_element = results_element.second.stats;
        const auto wrt_am = static_cast<double>(stats_element.std_dev_duration) / static_cast<double>(stats_element.am_duration)
            * 100.0;
        std::printf("    Spot \"%s\":\n"
                    "        Call count:      %10lu\n"
                    "        Arithmetic mean: %10.3f ms\n"
                    "        Geometric mean:  %10.3f ms\n"
                    "        Maximum:         %10.3f ms\n"
                    "        Minimum:         %10.3f ms\n"
                    "        Std. deviation:  %10.3f ms (%3.1f%% of mean).\n\n",
            results_element.first.c_str(),
            stats_element.exp_count,
            stats_element.am_duration,
            stats_element.gm_duration,
            stats_element.max_duration,
            stats_element.min_duration,
            stats_element.std_dev_duration,
            wrt_am);
    }
    
    // output percentages
    std::printf("+=================================================\n"
                " Total time of window: %10.3f ms\n"
                " Window breakdown: \n"
                "\n    %50s%15s%10s\n",
        total_time_window * 1E-3,
        "Spot name",
        "Call count",
        "% total");
    for (const auto& percentage : percentages)
    {
        auto iter_results = percentage.first;
        if (iter_results == results_.cend())
        {
            throw std::logic_error("tixl_spot_profiler_output_results_sorting_unknown_error");
        }
        std::printf("    %50s%15lu%9.1f%%\n", iter_results->first.c_str(), iter_results->second.stats.exp_count,
            percentage.second);
    }
    
    // footer
    std::printf("\n+=================================================\n");
}

//+//////////////////////////////////////////////
// implementation of spot_profiler -> access
//+//////////////////////////////////////////////

perf_stats spot_profiler::get_stats(const std::string& key) const
{
    // pre-conditions
    if (results_.empty())
    {
        throw std::runtime_error("tixl_spot_profiler_get_stats_results_unavailable_error");
    }
    
    auto iter_result = results_.find(key);
    if (iter_result == results_.cend())
    {
        throw std::runtime_error("tixl_spot_profiler_get_stats_unable_to_find_result_error");
    }
    return iter_result->second.stats;
}

//+//////////////////////////////////////////////
// implementation of spot_profiler -> implementation
//+//////////////////////////////////////////////

void spot_profiler::compute_results()
{
    // loop over spots
    const auto& spots = get_impl()->spots; // convenience
    for (const auto& spot : spots)
    {
        // pre-conditions
        const auto& times_count = spot.second.size();
        if (times_count < 1)
        {
            throw std::runtime_error("tixl_spot_profiler_compute_results_no_measurements_made_error");
        }
        
        // compute the times
        auto times = std::vector<std::uint64_t>();
        times.reserve(times_count);
        for (const auto& spot_time : spot.second)
        {
            const auto time_signed = static_cast<std::int64_t>(spot_time.second) - static_cast<std::int64_t>(spot_time.first);
            if (time_signed < 0)
            {
                throw std::runtime_error("tixl_spot_profiler_compute_results_negative_time_error");
            }
            times.push_back(static_cast<std::uint64_t>(time_signed));
        }
        if (times.size() != times_count)
        {
            throw std::runtime_error("tixl_spot_profiler_compute_results_could_not_compute_all_times_error");
        }
        
        // compute the statistics
        const auto stats = compute_statistics(times);
        
        // insert a statistics element
        auto rpack = results_pack{};
        rpack.times = std::move(times);
        rpack.stats = std::move(stats);
        auto [iter_insert, insert_success] = results_.insert(std::make_pair(spot.first, rpack));
        if (!insert_success)
        {
            throw std::runtime_error("tixl_spot_profiler_compute_results_could_not_insert_results_element_error");
        }
    }
    
    // post-conditions
    if (results_.size() != spots.size())
    {
        throw std::runtime_error("tixl_spot_profiler_compute_results_not_all_results_computed_error");
    }
}

} // namespace tixl

// END OF IMPLEMENTATION
