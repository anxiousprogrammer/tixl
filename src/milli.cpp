//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a utility which can be used to obtain a time-stamp in milliseconds.

#include "impl/spot_timer.h"

#include <cstdio> // std::printf

int main(int argc, char** argv)
{
    const auto now_since_epoch = tixl::impl::clock_t::now().time_since_epoch();
    const auto micro = std::chrono::duration_cast<std::chrono::microseconds>(now_since_epoch);
    const auto milli = static_cast<double>(micro.count()) * 1E-3;
    
    printf("%10.3f\n", milli);
    
    return 0;
}

// END OF IMPLEMENTATION
