//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation(s) of 'mpi_spot_profiler.h"

#include "mpi_spot_profiler.h"

#include "impl/spot_profiler_impl.h"
#include "compute_statistics.h"

#include <stdexcept> // std::runtime_error
#include <chrono> // std::system_clock
#include <ctime> // std::strftime
#include <cstdio> // std::printf
#include <vector> // std::vector
#include <algorithm> // std::sort
#include <type_traits> // std::remove_reference
#include <cstdint> // std::uint64_t
#include <limits> // std::numeric_limits
#include <cstdlib> // std::getenv

#include <mpi.h>

namespace tixl
{

//+//////////////////////////////////////////////
// implementation of mpi_spot_profiler -> lifecycle
//+//////////////////////////////////////////////

mpi_spot_profiler::mpi_spot_profiler()
{
    impl_ = new impl::spot_profiler_impl;
}

mpi_spot_profiler::~mpi_spot_profiler()
{
    if (impl_ != nullptr)
    {
        delete impl_;
        impl_ = nullptr;
    }
}

//+//////////////////////////////////////////////
// implementation of mpi_spot_profiler -> main functionality
//+//////////////////////////////////////////////

void mpi_spot_profiler::open_window()
{
    get_impl()->open_window();
}

mpi_spot_profiler::e_window_stage mpi_spot_profiler::get_window_stage() const
{
    switch(get_impl()->get_window_stage())
    {
        case impl::spot_profiler_impl::unopened:
        {
            return unopened;
        }
        case impl::spot_profiler_impl::opened:
        {
            return opened;
        }
        default:
        {
            break;
        }
    };
    
    return closed;
}

void mpi_spot_profiler::open_spot(const std::string& key)
{
    get_impl()->open_spot(key);
}

void mpi_spot_profiler::close_spot(const std::string& key)
{
    get_impl()->close_spot(key);
}

void mpi_spot_profiler::declare_unreachable_spot(const std::string& key)
{
    // find or insert spot
    auto& spots = get_impl()->spots;
    auto iter_spot = spots.find(key);
    if (iter_spot == spots.cend())
    {
        // insert
        auto [iter_insert, insert_success] = spots.insert(std::make_pair(key, impl::spot_profiler_impl::spot_t{}));
        if (!insert_success)
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_declare_unreachable_spot_unable_to_insert_new_spot_error");
        }
    }
}

void mpi_spot_profiler::close_window(MPI_Comm comm)
{
    // pre-conditions
    if (comm == MPI_COMM_NULL)
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_close_window_null_comm_error");
    }
    
    // close the window
    get_impl()->close_window();
    
    // duplicate world comm.
    auto new_comm = MPI_Comm{MPI_COMM_NULL};
    MPI_Comm_dup(comm, &new_comm);
    if (new_comm == MPI_COMM_NULL)
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_close_window_comm_dup_failed_error");
    }
    
    // compute the results
    compute_results(new_comm);
    
    // free the comm.
    if (MPI_Comm_free(&new_comm) != MPI_SUCCESS)
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_close_window_comm_free_unknown_error");
    }
}

void mpi_spot_profiler::output_results(const std::string& header) const
{
    // pre-conditions
    if (results_.empty())
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_output_results_results_unavailable_error");
    }
    
    // header
    const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    auto time_stamp_buffer = std::string(30, '\0');
    std::strftime(time_stamp_buffer.data(), time_stamp_buffer.size(), "%d.%m.%Y|%H:%M:%S", std::localtime(&time_stamp));
    std::printf("\n+=================================================\n"
                  " MPI Spot Profiler Output\n"
                  " Time stamp: %s\n"
                  " %s\n"
                  "+=================================================\n",
        time_stamp_buffer.c_str(),
        header.c_str());
    
    // quick exit
    if (results_.empty())
    {
        std::printf("    No spots available.\n"
                    "\n+=================================================\n");
        return;
    }
    
    // compute percentages
    using results_iter_t = std::remove_const<std::remove_reference<decltype(results_)>::type>::type::const_iterator;
    struct percentages_pack
    {
        double mean = {};
        double min = std::numeric_limits<double>::max();
        double max = {};
    };
    std::vector<std::pair<results_iter_t, percentages_pack>> percentages = {};
    percentages.reserve(results_.size());
    for (auto iter_results = results_.cbegin(); iter_results != results_.cend(); ++iter_results)
    {
        // convenience
        const auto& gathered_times = iter_results->second.gathered_times;
        const auto& gathered_times_counts = iter_results->second.gathered_times_counts;
        const auto& gathered_times_offsets = iter_results->second.gathered_times_offsets;
        
        // determine the worker count
        const auto& worker_count = gathered_times_counts.size();
        if (worker_count == 0)
        {
            throw std::logic_error("tixl_mpi_spot_profiler_output_results_zero_workers_error");
        }
        
        // compute the mean, min. and max. percentages of each spot, sampling across the MPI ranks
        auto ppack = percentages_pack{};
        auto non_contributing_worker_count = std::size_t{};
        for (auto worker_index = std::size_t{}; worker_index < worker_count; ++worker_index)
        {
            const auto begin_index = static_cast<std::size_t>(gathered_times_offsets[worker_index]);
            const auto end_index = begin_index + static_cast<std::size_t>(gathered_times_counts[worker_index]);
            auto total_time_worker = std::uint64_t{};
            for (auto time_index = begin_index; time_index < end_index; ++time_index)
            {
                total_time_worker += gathered_times[time_index];
            }
            const auto percentage = static_cast<double>(total_time_worker) / static_cast<double>(reduced_total_time_) * 100.0;
            if (percentage == 0.0)
            {
                ++non_contributing_worker_count; // ranks which do not reach spots must not contribute to the statistics
            }
            if (percentage > 100.0 + 1E-3)
            {
                throw std::logic_error("tixl_mpi_spot_profiler_output_results_percentage_greater_than_100_error");
            }
            
            // record
            ppack.mean += percentage;
            ppack.min = std::min(ppack.min, percentage);
            ppack.max = std::max(ppack.max, percentage);
        }
        if (non_contributing_worker_count == worker_count)
        {
            throw std::logic_error("tixl_mpi_spot_profiler_output_results_unvisited_spot_error");
        }
        ppack.mean /= static_cast<double>(worker_count - non_contributing_worker_count);
        
        // check the health of the info
        if (ppack.mean < ppack.min || ppack.mean > ppack.max || ppack.max < ppack.min)
        {
            throw std::logic_error("tixl_mpi_spot_profiler_output_results_invalid_mean_min_max_percentages_error");
        }
        
        // insert
        percentages.emplace_back(std::make_pair(iter_results, ppack));
    }
    
    // sort them
    std::sort(percentages.begin(), percentages.end(),
        [](const auto& v1, const auto& v2)
        {
            return v1.second.max > v2.second.max;
        });
    
    // output statistics
    for (const auto& percentage : percentages)
    {
        const auto& results_element = *percentage.first;
        const auto& stats_element = results_element.second.all_ranks_stats;
        const auto wrt_am = stats_element.std_dev_duration / stats_element.am_duration * 100.0;
        std::printf("    Spot \"%s\":\n"
                    "        Call count:      %10lu\n"
                    "        Arithmetic mean: %10.3f ms\n"
                    "        Geometric mean:  %10.3f ms\n"
                    "        Maximum:         %10.3f ms\n"
                    "        Minimum:         %10.3f ms\n"
                    "        Std. deviation:  %10.3f ms (%3.1f%% of mean).\n\n",
            results_element.first.c_str(),
            stats_element.exp_count,
            stats_element.am_duration,
            stats_element.gm_duration,
            stats_element.max_duration,
            stats_element.min_duration,
            stats_element.std_dev_duration,
            wrt_am);
    }
    
    // output percentages
    std::printf("+=================================================\n"
                " Total time of window: %10.3f ms\n"
                " Window breakdown: \n"
                "\n    %50s%15s%10s%10s%10s\n",
        static_cast<double>(reduced_total_time_) * 1E-3,
        "Spot name",
        "Call count",
        "% (mean)",
        "% (min.)",
        "% (max.)");
    for (const auto& percentage : percentages)
    {
        auto iter_stats = percentage.first;
        if (iter_stats == results_.cend())
        {
            throw std::logic_error("tixl_mpi_spot_profiler_output_results_sorting_unknown_error");
        }
        std::printf("    %50s%15lu%9.1f%%%9.1f%%%9.1f%%\n",
            iter_stats->first.c_str(),
            iter_stats->second.all_ranks_stats.exp_count,
            percentage.second.mean,
            percentage.second.min,
            percentage.second.max);
    }
    
    // footer
    std::printf("\n+=================================================\n");
}

void mpi_spot_profiler::write_results(const std::string& file_name_prefix) const
{
    // pre-conditions
    if (results_.empty())
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_write_results_results_unavailable_error");
    }
    
    // obtain the number of ranks
    const auto& rank_count = results_.begin()->second.gathered_times_counts.size();
    for (const auto& results_element : results_)
    {
        if (results_element.second.gathered_times_counts.size() != rank_count)
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_write_results_rank_count_inconsistency_between_spots_error");
        }
    }
    
    // loop through all ranks and write individual results
    // NOTE: the results of a maximum of 1000 ranks are written to one file and suffixing is used to differentiate the files
    constexpr auto file_limit = 1000;
    std::FILE* file = nullptr;
    for (auto ii = std::size_t{}; ii < rank_count; ++ii)
    {
        // open a new file if needed
        if (ii == 0 || ii % file_limit == 0)
        {
            // close if the file is already open
            if (file != nullptr)
            {
                if (std::fclose(file) != 0)
                {
                    throw std::runtime_error("tixl_mpi_spot_profiler_write_results_unable_to_close_target_file_error");
                }
                file = nullptr;
            }
            
            // open a new file
            const auto until_rank = ii + file_limit - 1;
            const auto file_name = file_name_prefix + "_ranks_" + std::to_string(ii) + "_to_"
                + std::to_string(rank_count < until_rank ? rank_count - 1 : until_rank);
            file = std::fopen(file_name.c_str(), "w");
            if (file == nullptr)
            {
                throw std::runtime_error("tixl_mpi_spot_profiler_write_results_unable_to_open_target_file_error");
            }
        }
        
        // header
        const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        auto time_stamp_buffer = std::string(30, '\0');
        std::strftime(time_stamp_buffer.data(), time_stamp_buffer.size(), "%d.%m.%Y|%H:%M:%S", std::localtime(&time_stamp));
        std::fprintf(file, "\n+=================================================\n"
                           " MPI Spot Profiler Output\n"
                           " Time stamp: %s\n"
                           " Statistics for rank %lu\n"
                           "+=================================================\n",
            time_stamp_buffer.c_str(),
            ii);
        
        // compute percentages
        using results_iter_t = std::remove_const<std::remove_reference<decltype(results_)>::type>::type::const_iterator;
        std::vector<std::pair<results_iter_t, double>> percentages = {};
        percentages.reserve(results_.size());
        for (auto iter_results = results_.cbegin(); iter_results != results_.cend(); ++iter_results)
        {
            // convenience
            const auto& gathered_times = iter_results->second.gathered_times;
            const auto& gathered_times_counts = iter_results->second.gathered_times_counts;
            const auto& gathered_times_offsets = iter_results->second.gathered_times_offsets;
            
            // compute the percentage and record it
            const auto begin_index = static_cast<std::size_t>(gathered_times_offsets[ii]);
            const auto end_index = begin_index + static_cast<std::size_t>(gathered_times_counts[ii]);
            auto total_time_worker = std::uint64_t{};
            for (auto time_index = begin_index; time_index < end_index; ++time_index)
            {
                total_time_worker += gathered_times[time_index];
            }
            const auto percentage = static_cast<double>(total_time_worker) / static_cast<double>(reduced_total_time_) * 100.0;
            if (percentage > 100.0 + 1E-3)
            {
                throw std::logic_error("tixl_mpi_spot_profiler_write_results_percentage_greater_than_100_error");
            }
            percentages.emplace_back(std::make_pair(iter_results, percentage));
        }
        
        // sort them
        std::sort(percentages.begin(), percentages.end(),
            [](const std::pair<results_iter_t, double>& v1, const std::pair<results_iter_t, double>& v2)
            {
                return v1.second > v2.second;
            });
        
        // output statistics
        for (const auto& percentage : percentages)
        {
            const auto& results_element = *percentage.first;
            const auto& stats_element = results_element.second.stats[ii];
            const auto wrt_am = stats_element.std_dev_duration / stats_element.am_duration * 100.0;
            std::fprintf(file, "    Spot \"%s\":\n"
                               "        Call count:      %10lu\n"
                               "        Arithmetic mean: %10.3f ms\n"
                               "        Geometric mean:  %10.3f ms\n"
                               "        Maximum:         %10.3f ms\n"
                               "        Minimum:         %10.3f ms\n"
                               "        Std. deviation:  %10.3f ms (%3.1f%% of mean).\n\n",
                results_element.first.c_str(),
                stats_element.exp_count,
                stats_element.am_duration,
                stats_element.gm_duration,
                stats_element.max_duration,
                stats_element.min_duration,
                stats_element.std_dev_duration,
                wrt_am);
        }
        
        // output percentages
        std::fprintf(file, "+=================================================\n"
                           " Total time of window: %10.3f ms\n"
                           " Window Breakdown for rank %lu: \n"
                           "\n    %50s%15s%10s\n",
            static_cast<double>(reduced_total_time_) * 1E-3,
            ii,
            "Spot name",
            "Call count",
            "% total");
        for (const auto& percentage : percentages)
        {
            auto iter_stats = percentage.first;
            if (iter_stats == results_.cend())
            {
                throw std::logic_error("tixl_mpi_spot_profiler_write_results_sorting_unknown_error");
            }
            std::fprintf(file, "    %50s%15lu%9.1f%%\n",
                iter_stats->first.c_str(),
                iter_stats->second.stats[ii].exp_count,
                percentage.second);
        }
        
        // footer
        std::fprintf(file, "\n+=================================================\n\n");
    }
    
    // close the file if it's open
    if (file != nullptr)
    {
        if (std::fclose(file) != 0)
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_write_results_unable_to_close_target_file_error");
        }
    }
}

//+//////////////////////////////////////////////
// implementation of mpi_spot_profiler -> access
//+//////////////////////////////////////////////

perf_stats mpi_spot_profiler::get_stats(const std::string& key, const unsigned rank) const
{
    // pre-conditions
    if (results_.empty())
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_get_stats_results_not_available_error");
    }
    
    auto iter_result = results_.find(key);
    if (iter_result == results_.cend())
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_get_stats_unable_to_find_result_error");
    }
    if (rank >= iter_result->second.stats.size())
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_get_stats_invalid_rank_error");
    }
    return iter_result->second.stats[rank];
}

perf_stats mpi_spot_profiler::get_all_ranks_stats(const std::string& key) const
{
    // pre-conditions
    if (results_.empty())
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_get_all_ranks_stats_results_not_available_error");
    }
    
    auto iter_result = results_.find(key);
    if (iter_result == results_.cend())
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_get_all_ranks_stats_unable_to_find_result_error");
    }
    return iter_result->second.all_ranks_stats;
}

//+//////////////////////////////////////////////
// implementation of mpi_spot_profiler -> implementation
//+//////////////////////////////////////////////

void mpi_spot_profiler::compute_results(MPI_Comm comm)
{
    // pre-conditions
    if (comm == MPI_COMM_NULL)
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_null_comm_error");
    }
    
    // get rank and size
    auto comm_rank = -1;
    MPI_Comm_rank(comm, &comm_rank);
    if (comm_rank == -1)
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_unable_to_get_rank_error");
    }
    auto comm_size = -1;
    MPI_Comm_size(comm, &comm_size);
    if (comm_size == -1)
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_unable_to_get_size_error");
    }
    
    // loop over spots
    const auto& spots = get_impl()->spots;
    for (const auto& spot : spots)
    {
        // reduce the spot's key across all ranks
        const auto& spot_key = spot.first;
        if (spot_key.size() > impl::g_max_key_length)
        {
            throw std::logic_error("tixl_mpi_spot_profiler_compute_results_key_too_long_error");
        }
        auto spot_key_send_buffer = std::string(impl::g_max_key_length, ' ');
        std::copy(spot_key.cbegin(), spot_key.cend(), spot_key_send_buffer.begin());
        auto spot_key_recv_buffer = std::string(impl::g_max_key_length, ' ');
        if (MPI_Allreduce(spot_key_send_buffer.data(), spot_key_recv_buffer.data(),
            static_cast<int>(spot_key_recv_buffer.size()), MPI_CHAR, MPI_MAX, comm) != MPI_SUCCESS)
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_allreduce_error");
        }
        
        // ensure that all ranks have an entry for this spot
        auto key_changed = false;
        for (auto ii = std::size_t{}; ii < impl::g_max_key_length; ++ii)
        {
            if (spot_key_send_buffer[ii] != spot_key_recv_buffer[ii])
            {
                key_changed = true;
            }
        }
        if (key_changed)
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_undeclared_unreachable_spot_error");
        }
        
        // compute the times
        auto times = std::vector<std::uint64_t>();
        times.reserve(spot.second.size());
        for (const auto& spot_time : spot.second)
        {
            const auto time_signed = static_cast<std::int64_t>(spot_time.second) - static_cast<std::int64_t>(spot_time.first);
            if (time_signed < 0)
            {
                throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_negative_time_error");
            }
            times.emplace_back(static_cast<std::uint64_t>(time_signed));
        }
        
        // gather sizes of times on all ranks to root (some may have not reached the spot at all)
        const auto times_count = static_cast<int>(times.size());
        auto gathered_times_counts = std::vector<int>{};
        if (comm_rank == 0)
        {
            gathered_times_counts.resize(comm_size);
        }
        if (MPI_Gather(&times_count, 1, MPI_INT, gathered_times_counts.data(), 1, MPI_INT, 0, comm) != MPI_SUCCESS)
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_gather_times_error");
        }
        
        // compute max. and total times count, check for false unreachable spots and compute offsets
        auto gathered_times_offsets = std::vector<int>{};
        auto total_times_count = std::size_t{};
        auto max_times_count = std::size_t{};
        if (comm_rank == 0)
        {
            // compute max. and total times count
            for (const auto& gathered_times_count : gathered_times_counts)
            {
                total_times_count += static_cast<std::size_t>(gathered_times_count);
                max_times_count = std::max(max_times_count, static_cast<std::size_t>(gathered_times_count));
            }
            
            // check for false unreachable spots
            if (total_times_count == 0)
            {
                throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_unvisited_spot_error");
            }
            
            // compute offsets
            gathered_times_offsets.resize(gathered_times_counts.size());
            gathered_times_offsets[0] = 0;
            for (auto ii = 1; ii < gathered_times_offsets.size(); ++ii)
            {
                gathered_times_offsets[ii] = gathered_times_offsets[ii - 1] + gathered_times_counts[ii - 1];
            }
        }
        
        // gather times to root
        auto gathered_times = std::vector<std::uint64_t>{};
        if (comm_rank == 0)
        {
            gathered_times = std::vector<std::uint64_t>(total_times_count);
        }
        if (MPI_Gatherv(times.data(), times.size(), MPI_UINT64_T, gathered_times.data(), gathered_times_counts.data(),
            gathered_times_offsets.data(), MPI_UINT64_T, 0, comm) != MPI_SUCCESS)
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_gatherv_error");
        }
        
        // insert a results element
        if (comm_rank == 0)
        {
            // compute statistics for each rank
            auto stats = std::vector<perf_stats>{};
            stats.reserve(gathered_times_counts.size());
            for (auto ii = std::size_t{}; ii < gathered_times_counts.size(); ++ii)
            {
                if (gathered_times_counts[ii] == 0)
                {
                    auto empty_stats_element = perf_stats{};
                    empty_stats_element.gm_duration = 0.0;
                    empty_stats_element.min_duration = 0.0;
                    empty_stats_element.max_duration = 0.0;
                    stats.emplace_back(empty_stats_element);
                }
                else
                {
                    auto this_rank_times = std::vector<std::uint64_t>(gathered_times_counts[ii]);
                    std::copy(gathered_times.cbegin() + gathered_times_offsets[ii],
                        gathered_times.cbegin() + gathered_times_offsets[ii] + gathered_times_counts[ii],
                        this_rank_times.begin());
                    stats.emplace_back(compute_statistics(this_rank_times));
                }
            }
            
            // compute statistics (all ranks)
            const auto all_ranks_stats = compute_statistics(gathered_times);
            
            // create a results element
            auto rpack = results_pack{};
            rpack.gathered_times = std::move(gathered_times);
            rpack.gathered_times_counts = std::move(gathered_times_counts);
            rpack.gathered_times_offsets = std::move(gathered_times_offsets);
            rpack.stats = std::move(stats);
            rpack.all_ranks_stats = std::move(all_ranks_stats);
            
            // insert
            auto [iter_insert, insert_success] = results_.insert(std::make_pair(spot.first, rpack));
            if (!insert_success)
            {
                throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_could_not_insert_stats_element_error");
            }
        }
    } // loop over spots
    
    // post-conditions
    if (comm_rank == 0)
    {
        if (results_.size() != spots.size())
        {
            throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_not_all_results_computed_error");
        }
    }
    else
    {
        if (results_.size() != 0)
        {
            throw std::logic_error("tixl_mpi_spot_profiler_compute_results_garbage_results_on_non_root_error");
        }
    }
    
    // reduce total time to obtain the total time of the window
    if (MPI_Reduce(&get_impl()->total_time, &reduced_total_time_, 1, MPI_UINT64_T, MPI_MAX, 0, comm) != MPI_SUCCESS)
    {
        throw std::runtime_error("tixl_mpi_spot_profiler_compute_results_reduce_total_time_error");
    }
}

} // namespace tixl

// END OF IMPLEMENTATION
