//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation(s) of 'output_results.h"

#include "output_results.h"

#include <stdexcept> // std::runtime_error
#include <chrono> // std::system_clock
#include <ctime> // std::strftime
#include <cstdio> // std::printf
#include <cmath> // std::ceil

namespace tixl
{

void output_results(
    const std::string& header,
    const perf_stats& results,
    const std::size_t& fp_count,
    const std::size_t& data_traffic)
{
    // preconditions
    if (results.am_duration < 0.0 || results.gm_duration < 0.0)
    {
        throw std::runtime_error("tixl_output_results_negative_mean_duration_error");
    }
    if (results.min_duration < 0.0)
    {
        throw std::runtime_error("tixl_output_results_negative_min_duration_error");
    }
    if (results.max_duration < 0.0)
    {
        throw std::runtime_error("tixl_output_results_negative_max_duration_error");
    }
    if (results.std_dev_duration < 0.0)
    {
        throw std::runtime_error("tixl_output_results_negative_std_dev_duration_error");
    }
    
    // standard report
    const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    auto time_stamp_buffer = std::string(30, '\0');
    std::strftime(time_stamp_buffer.data(), time_stamp_buffer.size(), "%d.%m.%Y|%H:%M:%S", std::localtime(&time_stamp));
    std::printf("+=================================================\n"
                "Time-stamp: %s\n"
                "%s\n"
                "+=================================================\n\n"
                "Number of experiments: %lu\n\n"
                "Arithmetic mean of duration: %10.3f ms\n"
                "Geometric mean of duration:  %10.3f ms\n"
                "Min. duration:               %10.3f ms\n"
                "Max. duration:               %10.3f ms\n"
                "Std. deviation in duration:  %10.3f ms (%4.1f%% of mean)\n",
                time_stamp_buffer.c_str(),
                header.c_str(),
                results.exp_count,
                results.am_duration,
                results.gm_duration,
                results.min_duration,
                results.max_duration,
                results.std_dev_duration,
                std::ceil(results.std_dev_duration / results.am_duration * 100.0));
    
    // traffic and work
    std::printf("\n");
    if (fp_count != 0)
    {
        std::printf("Total work:    %18lu FLOP (%10.7f GFLOP)\n", fp_count,
            static_cast<double>(fp_count) / static_cast<double>(1024 * 1024 * 1024));
    }
    if (data_traffic != 0)
    {
        std::printf("Total traffic: %18lu B    (%10.7f GiB)\n", data_traffic,
            static_cast<double>(data_traffic) / static_cast<double>(1024 * 1024 * 1024));
    }
    if (fp_count != 0 && data_traffic != 0)
    {
        const auto computational_intensity = static_cast<double>(fp_count) / static_cast<double>(data_traffic);
        std::printf("Computational intensity: %4.2f\n", computational_intensity);
    }
    
    // FLOP calculation
    if (fp_count != 0)
    {
        std::printf("\nArithmetic mean of FP performance: %10.1f GFLOP/s\n"
                      "Geometric mean of FP performance:  %10.1f GFLOP/s\n"
                      "Maximum FP performance:            %10.1f GFLOP/s\n"
                      "Minimum FP performance:            %10.1f GFLOP/s\n"
                      "Std. deviation in measurement:     %10.1f GFLOP/s\n",
                    static_cast<double>(fp_count) / results.am_duration * 1E-6,
                    static_cast<double>(fp_count) / results.gm_duration * 1E-6,
                    static_cast<double>(fp_count) / results.min_duration * 1E-6,
                    static_cast<double>(fp_count) / results.max_duration * 1E-6,
                    static_cast<double>(fp_count) * results.std_dev_inv_duration * 1E-6);
    }
    
    // BW calculation
    if (data_traffic != 0)
    {
        const auto data_traffic_gib = static_cast<double>(data_traffic) / static_cast<double>(1024 * 1024 * 1024);
        std::printf("\nArithmetic mean of bandwidth:  %10.1f GiB/s\n"
                      "Geometric mean of bandwidth:   %10.1f GiB/s\n"
                      "Maximum bandwidth:             %10.1f GiB/s\n"
                      "Minimum bandwidth:             %10.1f GiB/s\n"
                      "Std. deviation in measurement: %10.1f GiB/s\n",
                    data_traffic_gib / results.am_duration * 1E+3,
                    data_traffic_gib / results.gm_duration * 1E+3,
                    data_traffic_gib / results.min_duration * 1E+3,
                    data_traffic_gib / results.max_duration * 1E+3,
                    data_traffic_gib * results.std_dev_inv_duration * 1E+3);
    }
    std::printf("+=================================================\n\n");
}

} // namespace tixl

// END OF IMPLEMENTATION
