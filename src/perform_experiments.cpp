//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation(s) of 'perform_experiments.h"

#ifndef TIXL_USE_MPI
#include "perform_experiments.h"
#else
#include "mpi_perform_experiments.h"
#endif

#include "impl/spot_timer.h"

#include <stdexcept> // std::runtime_error
#include <iostream> // std::cout
#include <cmath> // std::ceil
#include <chrono> // std::system_clock
#include <iomanip> // std::put_time

#ifdef TIXL_USE_MPI
#include <mpi.h>
#endif

namespace tixl
{

#ifndef TIXL_USE_MPI
std::vector<std::uint64_t> perform_experiments(experiment_functor& exp_fn, const std::size_t& exp_count)
{
    // pre-conditions
    if (exp_count < 1)
    {
        throw std::runtime_error("tixl_perform_experiments_exp_count_error");
    }
    
    // perform warm-up runs
    const auto warm_up_count = exp_count / 5;
    for (auto iter_counter = std::size_t{}; iter_counter < warm_up_count; ++iter_counter)
    {
        exp_fn.init();
        exp_fn.perform_experiment();
        exp_fn.finish();
    }
    
    // progress 1
    #ifdef MAKE_PROGRESS_REPORTS
    {
        const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::cout << std::put_time(std::localtime(&time_stamp), "[%H:%M:%S]") << " warm-up completed.\n";
    }
    auto phase = 0;
    #endif
    
    // perform the timed experiments
    auto times = std::vector<std::uint64_t>{};
    auto timer = impl::spot_timer{};
    for (auto exp_counter = std::size_t{}; exp_counter < exp_count; ++exp_counter)
    {
        // init the data
        exp_fn.init();
        
        // perform a timed run
        timer.reset();
        exp_fn.perform_experiment();
        const auto run_duration = timer.duration_in_us();
        times.push_back(run_duration);
        
        // clean up
        exp_fn.finish();
        
        // progress 2
        #ifdef MAKE_PROGRESS_REPORTS
        const auto progress_percent = static_cast<std::size_t>(std::ceil(static_cast<double>(exp_counter)
            / static_cast<double>(exp_count) * 100.0));
        if (progress_percent > 25 && phase == 0
            || progress_percent > 50 && phase == 1
            || progress_percent > 75 && phase == 2)
        {
            const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
            std::cout << std::put_time(std::localtime(&time_stamp), "[%H:%M:%S]") << " progress just passed "
                << progress_percent - 1 << "%.\n";
            ++phase;
        }
        #endif
        
        // check if we're done here
        if (exp_counter == exp_count)
        {
            break;
        }
    }
    
    // post-conditions
    if (times.size() != exp_count)
    {
        throw std::logic_error("tixl_perform_experiments_not_all_experiments_performed_error");
    }
    
    return times;
}

#else

std::vector<std::uint64_t> mpi_perform_experiments(MPI_Comm comm, experiment_functor& exp_fn, const std::size_t& exp_count)
{
    // pre-conditions
    if (exp_count < 1)
    {
        throw std::runtime_error("tixl_mpi_perform_experiments_exp_count_error");
    }
    auto is_init = int{};
    MPI_Initialized(&is_init);
    if (is_init != 1)
    {
        throw std::logic_error("tixl_mpi_perform_experiments_mpi_not_initialized_error");
    }
    auto proc_rank = -1;
    MPI_Comm_rank(comm, &proc_rank);
    if (proc_rank == -1)
    {
        throw std::runtime_error("tixl_mpi_perform_experiments_mpi_comm_rank_error");
    }
    
    // perform warm-up runs
    const auto warm_up_count = exp_count / 5;
    for (auto iter_counter = std::size_t{}; iter_counter < warm_up_count; ++iter_counter)
    {
        exp_fn.init();
        exp_fn.perform_experiment();
        exp_fn.finish();
    }
    
    // progress 1
    #ifdef MAKE_PROGRESS_REPORTS
    if (proc_rank == 0)
    {
        const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::cout << std::put_time(std::localtime(&time_stamp), "[%H:%M:%S]") << " warm-up completed." << "\n";
    }
    auto phase = 0;
    #endif
    
    // perform the timed experiments
    auto times = std::vector<std::uint64_t>{};
    auto timer = impl::spot_timer{};
    for (auto exp_counter = std::size_t{}; exp_counter < exp_count; ++exp_counter)
    {
        // init the data
        exp_fn.init();
        
        // perform a timed run
        timer.reset();
        exp_fn.perform_experiment();
        MPI_Barrier(comm); // prevent inter-experiment parallelism!
        const auto run_duration = timer.duration_in_us();
        times.push_back(run_duration);
        
        // clean up
        exp_fn.finish();
        
        // progress 2
        #ifdef MAKE_PROGRESS_REPORTS
        const auto progress_percent = static_cast<std::size_t>(std::ceil(static_cast<double>(exp_counter + 1)
            / static_cast<double>(exp_count) * 100.0));
        if (progress_percent > 25 && phase == 0
            || progress_percent > 50 && phase == 1
            || progress_percent > 75 && phase == 2)
        {
            MPI_Barrier(comm);
            if (proc_rank == 0)
            {
                const auto time_stamp = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
                std::cout << std::put_time(std::localtime(&time_stamp), "[%H:%M:%S]") << " progress just passed "
                    << progress_percent - 1 << "%.\n";
            }
            ++phase;
        }
        #endif
    }
    
    // post-conditions
    if (times.size() != exp_count)
    {
        throw std::logic_error("tixl_mpi_perform_experiments_not_all_experiments_performed_error");
    }
    
    return times;
}
#endif

} // namespace tixl

// END OF IMPLEMENTATION
