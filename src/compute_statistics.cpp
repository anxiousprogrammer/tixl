//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation(s) of 'compute_statistics.h' and 'mpi_compute_statistics.h'

#include "compute_statistics.h"
#ifdef TIXL_USE_MPI
#include "mpi_compute_statistics.h"
#endif

#include <stdexcept> // std::runtime_error
#include <cmath> // std::fabs
#include <limits> // std::numeric_limits
#include <algorithm> // std::max

#ifdef TIXL_USE_MPI
#include <mpi.h>
#endif

namespace tixl
{

perf_stats compute_statistics(const std::vector<std::uint64_t>& times)
{
    // preconditions
    if (times.empty())
    {
        throw std::invalid_argument("tixl_compute_statistics_no_measurements_error");
    }
    
    // create an output instance
    auto results = perf_stats{};
    results.exp_count = times.size();
    
    // means and min/max
    auto sum = std::uint64_t{};
    auto am_inv_duration = 0.0;
    auto gm_mantissa = 1.0;
    auto gm_exponent = 0.0;
    auto min_duration = std::numeric_limits<std::uint64_t>::max();
    auto max_duration = std::uint64_t{};
    for (const auto& time : times)
    {
        // sanity check
        if (time < 0.0)
        {
            throw std::runtime_error("tixl_compute_statistics_negative_measurement_error");
        }
        
        // arithmetic mean
        sum += time;
        
        // arithmetic mean of the inverse of duration
        am_inv_duration += 1.0 / static_cast<double>(time);
        
        // geometric mean
        auto exponent = 1;
        const auto mantissa = std::frexp(time, &exponent);
        gm_mantissa *= mantissa;
        gm_exponent += exponent;
        
        // min/max
        min_duration = std::min(min_duration, time);
        max_duration = std::max(max_duration, time);
    }
    const auto inv_n = 1.0 / static_cast<double>(times.size());
    results.am_duration = static_cast<double>(sum) * inv_n * 1E-3;
    am_inv_duration *= inv_n * 1E+3;
    results.gm_duration = std::pow(gm_mantissa, inv_n)
        * std::pow(std::numeric_limits<double>::radix, gm_exponent * inv_n) * 1E-3;
    results.min_duration = static_cast<double>(min_duration) * 1E-3;
    results.max_duration = static_cast<double>(max_duration) * 1E-3;
    
    // standard deviations
    for (const auto& time : times)
    {
        const auto time_ms = static_cast<double>(time) * 1E-3;
        const auto variance_duration = std::fabs(time_ms - results.am_duration);
        results.std_dev_duration += variance_duration * variance_duration;
        
        const auto variance_inv_duration = std::fabs(1.0 / time_ms - am_inv_duration);
        results.std_dev_inv_duration += variance_inv_duration * variance_inv_duration;
    }
    results.std_dev_duration = std::sqrt(results.std_dev_duration * inv_n);
    results.std_dev_inv_duration = std::sqrt(results.std_dev_inv_duration * inv_n);
    
    return results;
}

#ifdef TIXL_USE_MPI
perf_stats mpi_compute_statistics(MPI_Comm comm, const std::vector<std::uint64_t>& times)
{
    // pre-condition
    if (times.empty())
    {
        throw std::runtime_error("tixl_mpi_compute_statistics_no_measurements_error");
    }
    
    // reduce to root
    auto reduced_times = std::vector<std::uint64_t>(times.size());
    if (MPI_Reduce(times.data(), reduced_times.data(), times.size(), MPI_UINT64_T, MPI_MAX, 0, comm) != MPI_SUCCESS)
    {
        throw std::runtime_error("tixl_mpi_compute_statistics_reduce_error");
    }
    
    // only root proceeds from here
    auto rank = -1;
    MPI_Comm_rank(comm, &rank);
    if (rank == -1)
    {
        throw std::runtime_error("tixl_tixl_mpi_compute_statistics_could_not_obtain_rank_error");
    }
    if (rank != 0)
    {
        return {};
    }
    
    // root may now proceed to calculate the statistical info.
    return compute_statistics(reduced_times);
}
#endif

} // namespace tixl

// END OF IMPLEMENTATION
