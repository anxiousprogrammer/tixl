//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the common implementation of spot_profiler and mpi_spot_profiler.

#ifndef TIXL_IMPL_SPOT_PROFILER_IMPL_H
#define TIXL_IMPL_SPOT_PROFILER_IMPL_H

#include "spot_timer.h"

#include <vector> // std::vector
#include <string> // std::string
#include <map> // std::map
#include <cstdint> // std::uint64_t

namespace tixl
{
namespace impl
{

constexpr std::size_t g_max_key_length = 100;

class spot_profiler_impl
{
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    spot_profiler_impl() = default;
    
    spot_profiler_impl(const spot_profiler_impl&) = default;
    
    spot_profiler_impl(spot_profiler_impl&&) = default;
    
    spot_profiler_impl& operator=(const spot_profiler_impl&) = default;
    
    spot_profiler_impl& operator=(spot_profiler_impl&&) = default;
    
    ~spot_profiler_impl() = default;
       
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void open_window();
    
    void open_spot(const std::string& key);
    
    void close_spot(const std::string& key);
    
    void close_window();
    
    //+/////////////////
    // access
    //+/////////////////
    
    enum e_window_stage { unopened, opened, closed };
    
    inline e_window_stage get_window_stage() const
    {
        return window_stage_;
    }
    
private:
    
    //+/////////////////
    // members
    //+/////////////////
    
    spot_timer spot_timer_ = {};
    
    e_window_stage window_stage_ = unopened;
    
public:
    
    std::uint64_t total_time = {};
    
    using spot_t = std::vector<std::pair<std::uint64_t, std::uint64_t>>;
    std::map<std::string, spot_t> spots = {};
};

} // namespace impl
} // namespace tixl

#endif // TIXL_IMPL_SPOT_PROFILER_IMPL_H
