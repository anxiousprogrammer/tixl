//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the clock class template of the TiXL library.

#ifndef TIXL_SPOT_TIMER_H
#define TIXL_SPOT_TIMER_H

#include <chrono> // std::chrono::time_point, std::chrono::high_resolution_clock etc...
#include <ratio> // std::micro
#include <type_traits> // std::conditional
#include <cstdint> // std::uint64_t

namespace tixl
{
namespace impl
{

//+//////////////////////////////////////////////
// spot-timer (replaces e.g. omp_get_wtime)
// advantage: we know that we're using a monotonic
// clock, we can request a minimum tick-period
//+//////////////////////////////////////////////

// clock type is public info
using clock_t = std::conditional<std::chrono::high_resolution_clock::is_steady,
    std::chrono::high_resolution_clock, std::chrono::steady_clock>::type;

class spot_timer
{      
public:

    //+//////////////////////////////////////
    // Lifecycle
    //+//////////////////////////////////////
    
    inline spot_timer()
    {
        static_assert(static_cast<double>(std::micro::num) / static_cast<double>(std::micro::den) >=
            static_cast<double>(clock_t::period::num) / static_cast<double>(clock_t::period::den));
     
        // record the start-time
        start_ = clock_t::now();
    }
    
    
    //+//////////////////////////////////////
    // Access
    //+//////////////////////////////////////
    
    inline void reset()
    {
        start_ = clock_t::now();
    }
    
    inline std::uint64_t duration_in_us() const
    {
        const auto time = clock_t::now() - start_;
        const auto time_us = std::chrono::duration_cast<std::chrono::microseconds>(time).count();
        return static_cast<std::uint64_t>(time_us);
    }
    
    
private:

    //+//////////////////////////////////////
    // Member
    //+//////////////////////////////////////
    
    std::chrono::time_point<clock_t> start_;
};

} // namespace impl
} // namespace tixl
#endif
