//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation(s) of 'spot_profiler_impl.h"

#include "spot_profiler_impl.h"
#include "spot_timer.h"

#include <stdexcept> // std::runtime_error

namespace tixl
{
namespace impl
{

//+//////////////////////////////////////////////
// implementation of spot_profiler_impl -> main functionality
//+//////////////////////////////////////////////

void spot_profiler_impl::open_window()
{
    // pre-condition
    if (window_stage_ == opened)
    {
        throw std::runtime_error("tixl_spot_profiler_impl_open_window_already_open_error");
    }
    if (window_stage_ == closed)
    {
        throw std::runtime_error("tixl_spot_profiler_impl_open_window_reopening_not_possible_error");
    }
    
    // fly the flag
    window_stage_ = opened;
    
    // reset the spot timer's clock
    spot_timer_.reset();
}

void spot_profiler_impl::open_spot(const std::string& key)
{
    // pre-condition
    if (window_stage_ != opened)
    {
        throw std::runtime_error("tixl_spot_profiler_impl_open_spot_no_open_window_error");
    }
    if (key.empty())
    {
        throw std::runtime_error("tixl_spot_profiler_impl_open_spot_empty_key_error");
    }
    if (key.size() > g_max_key_length)
    {
        throw std::runtime_error("tixl_spot_profiler_impl_open_spot_key_too_long_error");
    }
    
    // find or insert spot
    auto iter_spot = spots.find(key);
    if (iter_spot == spots.cend())
    {
        // insert
        auto [iter_insert, insert_success] = spots.insert(std::make_pair(key, spot_t{}));
        if (!insert_success)
        {
            throw std::runtime_error("tixl_spot_profiler_impl_open_spot_unable_to_insert_new_spot_error");
        }
        iter_spot = iter_insert;
    }
    
    // insert new time element and only then record the time
    iter_spot->second.push_back(std::pair<std::uint64_t, std::uint64_t>{});
    iter_spot->second.back().first = spot_timer_.duration_in_us();
}

void spot_profiler_impl::close_spot(const std::string& key)
{
    // capture time stamp
    const auto closing_time = spot_timer_.duration_in_us();
    
    // pre-condition
    if (window_stage_ != opened)
    {
        throw std::runtime_error("tixl_spot_profiler_impl_close_spot_no_open_window_error");
    }
    
    // find the spot
    auto iter_spot = spots.find(key);
    if (iter_spot == spots.cend())
    {
        throw std::runtime_error("tixl_spot_profiler_impl_open_spot_unable_to_find_spot_error");
    }
    
    // record the time
    iter_spot->second.back().second = closing_time;
}

void spot_profiler_impl::close_window()
{
    // pre-conditions
    if (window_stage_ != opened)
    {
        throw std::runtime_error("tixl_spot_profiler_impl_close_window_not_yet_opened_error");
    }
    
    // total time
    total_time = spot_timer_.duration_in_us();
    if (total_time == 0)
    {
        throw std::runtime_error("tixl_spot_profiler_impl_close_zero_total_time_error");
    }
    
    // bring down the flag
    window_stage_ = closed;
}

} // namespace impl
} // namespace tixl

// END OF IMPLEMENTATION
