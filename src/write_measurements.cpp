//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains the implementation(s) of 'write_measurements.h"

#include "write_measurements.h"

#include <fstream> // std::ofstream
#include <iostream> // std::cerr
#include <cstdint> // std::uint64_t

namespace tixl
{

//+//////////////////////////////////////////////
// implementation
//+//////////////////////////////////////////////

void write_measurements(const std::vector<std::uint64_t>& measurements, const std::string& filename)
{
    // preconditions
    if (measurements.empty())
    {
        std::cerr << "No measurement data available." << std::endl;
        return;
    }
    
    // open file
    auto file = std::ofstream(filename, std::ios_base::trunc);
    if (!file.is_open())
    {
        std::cerr << "Could not open file to which the measurements were to be written." << std::endl;
        return;
    }
    
    // write to file
    auto counter = std::size_t{1};
    for (const auto& measurement : measurements)
    {
        file << counter++ << " " << measurement << std::endl;
    }
    
    // close file
    file.close();
    if (file.is_open())
    {
        std::cerr << "Could not close file to which the measurements were to be written." << std::endl;
        return;
    }
}

} // namespace tixl
