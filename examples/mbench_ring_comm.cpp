//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a microbenchmark program for MPI-based ring communication.

#include "tixl_w_mpi.h"
#include "get_problem_size.h"
#include "schedule_thread_work_chunkwise.h"

#include <iostream> // std::cerr
#include <string> // std::string
#include <vector> // std::vector
#include <random> // std::mt19937
#include <algorithm> // std::find
#include <limits> // std::numeric_limits

#include <stdlib.h> // posix_memalign
#include <unistd.h> // sysconf

#include <immintrin.h> // _mm_clflush

#include <omp.h>
#include <mpi.h>

//+////////////////////////////////////////////////////////////////////////////////////////////////
// functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace
{

class ring_comm_ef : public tixl::experiment_functor
{
public:
    
    enum e_comm_mode { sync, async };
    
private:
    
    //+/////////////////
    // members
    //+/////////////////
    
    const bool no_comm_test_;
    const bool comm_only_test_;
    e_comm_mode send_mode_;
    e_comm_mode recv_mode_;
    
    const std::size_t n_;
    const unsigned rank_;
    const unsigned size_;
    
    double* recv_buffer1_ = nullptr;
    double* recv_buffer2_ = nullptr;
    double* send_buffer_ = nullptr;
    double* work_buffer_ = nullptr;
    
    std::vector<std::mt19937> random_engines_ = {};
    
    // is _mm_clflush available?
    bool use_clflush_ = true;
    
    // cache
    const bool is_first_;
    const bool is_last_;
    std::vector<MPI_Request> requests_ = {};
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    ring_comm_ef(
        const bool no_comm_test,
        const bool comm_only_test,
        const std::size_t& n,
        const e_comm_mode send_mode,
        const e_comm_mode recv_mode,
        const int rank,
        const int size)
            : no_comm_test_(no_comm_test),
            comm_only_test_(comm_only_test),
            n_(n),
            send_mode_(send_mode),
            recv_mode_(recv_mode),
            rank_(static_cast<unsigned>(rank)),
            size_(static_cast<unsigned>(size)),
            is_first_(rank_ == 0),
            is_last_(rank_ == size_ - 1)
    {
        // pre-conditions
        if (no_comm_test_ && comm_only_test_)
        {
            std::cerr << "Fatal error: no-comm and comm-only flags cannot be simultaneously true." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        
        // create the requests
        auto request_count = std::size_t{2};
        if (send_mode_ == async && recv_mode == async)
        {
            if (!is_first_ && !is_last_)
            {
                request_count = 4;
            }
        }
        else
        {
            if (is_first_ || is_last_)
            {
                request_count = 1;
            }
        }
        requests_ = std::vector(request_count, MPI_REQUEST_NULL);
        
        // seed the random engines
        if (!comm_only_test_)
        {
            const auto thread_count = static_cast<unsigned>(omp_get_max_threads());
            random_engines_.resize(thread_count);
            for (auto ti = unsigned{}; ti < thread_count; ++ti)
            {
                random_engines_[ti].seed(ti);
            }
        }
        
        // check to see if CLFLUSH is available
        if (!__builtin_cpu_supports("sse2"))
        {
            if (rank_ == 0)
            {
                std::cerr << "Warning! The CPU doesn't have the \'sse2\' flag, a feature of which is used to "
                    "forcefully evict cache lines to memory." << std::endl;
            }
            use_clflush_ = false;
        }
    }
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocation
        const auto page_size = sysconf(_SC_PAGESIZE);
        if (!no_comm_test_)
        {
            const auto alloc_status0 = posix_memalign(reinterpret_cast<void**>(&recv_buffer1_), page_size,
                n_ * sizeof(double));
            const auto alloc_status1 = posix_memalign(reinterpret_cast<void**>(&recv_buffer2_), page_size,
                n_ * sizeof(double));
            const auto alloc_status2 = posix_memalign(reinterpret_cast<void**>(&send_buffer_), page_size,
                n_ * sizeof(double));
            
            if (alloc_status0 != 0 || alloc_status1 != 0 || alloc_status2 != 0 || recv_buffer1_ == nullptr
                || recv_buffer2_ == nullptr || send_buffer_ == nullptr)
            {
                std::cerr << "Fatal error: failed to allocate comm. memory." << std::endl;
                MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
            }
        }
        if (!comm_only_test_)
        {
            const auto alloc_status3 = posix_memalign(reinterpret_cast<void**>(&work_buffer_), page_size,
                n_ * sizeof(double));
            
            if (alloc_status3 != 0 || work_buffer_ == nullptr)
            {
                std::cerr << "Fatal error: failed to allocate work memory." << std::endl;
                MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
            }
        }
        
        // first-touch
        const auto page_size_double = page_size / sizeof(double);
        const auto cache_line_size_double = TIXL_CACHE_LINE_SIZE / sizeof(double);
        #pragma omp parallel
        {
            const auto thread_work_plan = schedule_thread_work_chunkwise(n_);
            for (auto ii = thread_work_plan.work_interval.first; ii < thread_work_plan.work_interval.second;
                ii += page_size_double)
            {
                // page fault
                constexpr auto modifier_value = std::numeric_limits<double>::max();
                if (!no_comm_test_)
                {
                    recv_buffer1_[ii] = modifier_value;
                    recv_buffer2_[ii] = modifier_value;
                    send_buffer_[ii] = modifier_value;
                }
                if (!comm_only_test_)
                {
                    work_buffer_[ii] = modifier_value;
                }
                
                // flush
                if (use_clflush_)
                {
                    for (auto jj = std::size_t{}; jj < page_size_double; jj += cache_line_size_double)
                    {
                        if (!no_comm_test_)
                        {
                            _mm_clflush(recv_buffer1_ + ii + jj);
                            _mm_clflush(recv_buffer2_ + ii + jj);
                            _mm_clflush(send_buffer_ + ii + jj);
                        }
                        if (!comm_only_test_)
                        {
                            _mm_clflush(work_buffer_ + ii + jj);
                        }
                    }
                }
            }
        }
    }
    
    void perform_experiment() final
    {
        if (no_comm_test_)
        {
            work();
            return;
        }
        
        if (recv_mode_ == sync && send_mode_ == async)
        {
            sync_recv_async_send();
        }
        else if (recv_mode_ == async && send_mode_ == sync)
        {
            async_recv_sync_send();
        }
        else if (recv_mode_ == async && send_mode_ == async)
        {
            async_recv_async_send();
        }
    }
    
    void finish() final
    {
        if (!no_comm_test_)
        {
            std::free(recv_buffer1_);
            std::free(recv_buffer2_);
            std::free(send_buffer_);
            recv_buffer1_ = nullptr;
            recv_buffer2_ = nullptr;
            send_buffer_ = nullptr;
        }
        if (!comm_only_test_)
        {
            std::free(work_buffer_);
            work_buffer_ = nullptr;
        }
        
        for (auto& request : requests_)
        {
            request = MPI_REQUEST_NULL;
        }
    }

private:
    
    //+/////////////////
    // implementation
    //+/////////////////
    
    void work()
    {
        #pragma omp parallel
        {
            auto& random_engine = random_engines_[omp_get_thread_num()];
            auto dist = std::uniform_real_distribution<double>(-1.0, 1.0);
            const auto thread_work_plan = schedule_thread_work_chunkwise(n_);
            for (auto ii = thread_work_plan.work_interval.first; ii < thread_work_plan.work_interval.second; ++ii)
            {
                work_buffer_[ii] = dist(random_engine);
            }
        }
    }
    
    static constexpr int leftward_send_tag = 0;
    static constexpr int rightward_send_tag = 1;
    
    void sync_recv_async_send()
    {
        // post sends
        auto request_index = std::size_t{};
        if (!is_first_)
        {
            MPI_Isend(send_buffer_, n_, MPI_DOUBLE, rank_ - 1, leftward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        if (!is_last_)
        {
            MPI_Isend(send_buffer_, n_, MPI_DOUBLE, rank_ + 1, rightward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        
        // do work
        if (!comm_only_test_)
        {
            work();
        }
        
        // receive
        if (!is_last_)
        {
            MPI_Recv(recv_buffer1_, n_, MPI_DOUBLE, rank_ + 1, leftward_send_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        if (!is_first_)
        {
            MPI_Recv(recv_buffer2_, n_, MPI_DOUBLE, rank_ - 1, rightward_send_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }
        
        // wait
        MPI_Waitall(requests_.size(), requests_.data(), MPI_STATUSES_IGNORE);
    }
    
    void async_recv_sync_send()
    {
        // post receives
        auto request_index = std::size_t{};
        if (!is_first_)
        {
            MPI_Irecv(recv_buffer2_, n_, MPI_DOUBLE, rank_ - 1, rightward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        if (!is_last_)
        {
            MPI_Irecv(recv_buffer1_, n_, MPI_DOUBLE, rank_ + 1, leftward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        
        // do work
        if (!comm_only_test_)
        {
            work();
        }
        
        // send
        if (!is_last_)
        {
            MPI_Send(send_buffer_, n_, MPI_DOUBLE, rank_ + 1, rightward_send_tag, MPI_COMM_WORLD);
        }
        if (!is_first_)
        {
            MPI_Send(send_buffer_, n_, MPI_DOUBLE, rank_ - 1, leftward_send_tag, MPI_COMM_WORLD);
        }
        
        // wait
        MPI_Waitall(requests_.size(), requests_.data(), MPI_STATUSES_IGNORE);
    }
    
    void async_recv_async_send()
    {
        // post receives
        auto request_index = std::size_t{};
        if (!is_first_)
        {
            MPI_Irecv(recv_buffer1_, n_, MPI_DOUBLE, rank_ - 1, rightward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        if (!is_last_)
        {
            MPI_Irecv(recv_buffer2_, n_, MPI_DOUBLE, rank_ + 1, leftward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        
        // post sends
        if (!is_first_)
        {
            MPI_Isend(send_buffer_, n_, MPI_DOUBLE, rank_ - 1, leftward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        if (!is_last_)
        {
            MPI_Isend(send_buffer_, n_, MPI_DOUBLE, rank_ + 1, rightward_send_tag, MPI_COMM_WORLD,
                &requests_[request_index++]);
        }
        
        // do work
        if (!comm_only_test_)
        {
            work();
        }
        
        // wait
        MPI_Waitall(requests_.size(), requests_.data(), MPI_STATUSES_IGNORE);
    }
};

struct options_pack
{
    std::size_t n = {};
    bool no_comm_test = false;
    bool comm_only_test = false;
    ring_comm_ef::e_comm_mode send_mode = ring_comm_ef::async;
    ring_comm_ef::e_comm_mode recv_mode = ring_comm_ef::async;
};

options_pack parse_command_line_args(int argc, char** argv, int rank)
{
    // pre-conditions
    if (argc < 7)
    {
        std::cerr << "Fatal error: invalid number of words in argv." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // copy args into a string
    auto args = std::vector<std::string>{};
    args.reserve(argc);
    for (auto ii = 0u; ii < static_cast<unsigned>(argc); ++ii)
    {
        args.emplace_back(std::string(argv[ii]));
    }
    
    // look for optional args
    auto options = options_pack{};
    if (args.size() > 7)
    {
        // no comm test
        auto iter_pt = std::find(args.cbegin(), args.cend(), "--no-comm");
        if (iter_pt == args.cend())
        {
            iter_pt = std::find(args.cbegin(), args.cend(), "-nc");
        }
        if (iter_pt != args.cend())
        {
            options.no_comm_test = true;
            args.erase(iter_pt);
        }
        
        // comm-only test
        iter_pt = std::find(args.cbegin(), args.cend(), "--comm-only");
        if (iter_pt == args.cend())
        {
            iter_pt = std::find(args.cbegin(), args.cend(), "-co");
        }
        if (iter_pt != args.cend())
        {
            options.comm_only_test = true;
            args.erase(iter_pt);
        }
        
        // it can't be a no-comm AND a comm-only test
        if (options.no_comm_test && options.comm_only_test)
        {
            std::cerr << "Fatal error: can't run a no-comm and a comm-only test simultaneously." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    if (args.size() > 7)
    {
        std::cerr << "Fatal error: unknown optional argument present." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // map of required args
    auto required_args = std::map<std::string, bool>{{"-n", true}, {"-r", true}, {"-s", true}};
    if (options.no_comm_test)
    {
        required_args.find("-r")->second = false;
        required_args.find("-s")->second = false;
    }
    
    // look for required ones
    auto found_args = std::map<std::string, bool>{{"-n", false}, {"-r", false}, {"-s", false}};
    for (auto ii = std::size_t{1}; ii < args.size(); ii += 2)
    {
        const auto arg = args[ii];
        if (arg == "-n")
        {
            found_args.find("-n")->second = true;
            auto n_mib = 0.0;
            try
            {
                n_mib = std::stod(args[ii + 1]);
            }
            catch(...)
            {
                std::cerr << "Fatal error: could not parse a valid data size." << std::endl;
                MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
            }
            if (n_mib <= 0.0)
            {
                std::cerr << "Fatal error: negative or zero-valued data size." << std::endl;
                MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
            }
            options.n = static_cast<std::size_t>(n_mib * static_cast<double>(1024 * 1024)) / sizeof(double);
        }
        else if (arg == "-r" || arg == "--recv")
        {
            found_args.find("-r")->second = true;
            const auto& value = args[ii + 1];
            if (value == "async" || value == "a")
            {
                options.recv_mode = ring_comm_ef::async;
            }
            else if (value != "sync" && value != "s")
            {
                std::cerr << "Fatal error: unknown option for \"--recv\"." << std::endl;
                MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
            }
        }
        else if (arg == "-s" || arg == "--send")
        {
            found_args.find("-s")->second = true;
            const auto& value = args[ii + 1];
            if (value == "async" || value == "a")
            {
                options.send_mode = ring_comm_ef::async;
            }
            else if (value != "sync" && value != "s")
            {
                std::cerr << "Fatal error: unknown option for \"--send\"." << std::endl;
                MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
            }
        }
        else
        {
            std::cerr << "Fatal error: unknown input argument." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    if (options.send_mode == ring_comm_ef::sync && options.recv_mode == ring_comm_ef::sync)
    {
        std::cerr << "Fatal error: sync. receiving and sync. sending leads to a deadlock and is not supported."
            << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // post-conditions
    if (required_args.size() != found_args.size())
    {
        std::cerr << "Fatal error: required args container size is not equal to found args container size."
            << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    auto iter_ra = required_args.cbegin();
    for (auto iter_fa = found_args.cbegin(); iter_fa != found_args.cend(); ++iter_fa, ++iter_ra)
    {
        if (iter_ra->second && !iter_fa->second)
        {
            std::cerr << "Fatal error: not all required arguments were provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
    }
    
    return options;
}

} // anonymous namespace


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    // init mpi
    MPI_Init(&argc, &argv);
    
    // rank and size
    auto rank = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    auto size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    if (rank == -1 || size == -1)
    {
        std::cerr << "Fatal error: could not get world rank or size." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    if (size < 2)
    {
        std::cerr << "Fatal error: the program needs to be run with at least 2 ranks." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // extract args
    if (argc < 7 || std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help")
    {
        if (rank == 0)
        {
            std::cout << "Provide the following arguments in any order:\n"
                      << "    -n N                     size in MiB of data to be communicated,\n"
                      << "    --send|-s MODE           MODE may be 'async' or 'a', or 'sync' or 's',\n"
                      << "    --recv|-r MODE           MODE may be 'async' or 'a', or 'sync' or 's',\n"
                      << "    [--no-comm|-nc]          optional argument flags no-comm test, and,\n"
                      << "    [--comm-only|-co]        optional argument flags comm-only test." << std::endl;
        }
        MPI_Finalize();
        return 0;
    }
    const auto options = parse_command_line_args(argc, argv, rank);
    
    // report
    if (rank == 0)
    {
        std::cout << "Starting experiments with n = "
            << static_cast<double>(options.n * sizeof(double)) / static_cast<double>(1024 * 1024) << " MiB and "
            << omp_get_max_threads() << " threads." << std::endl;
    }
    
    // experiments
    auto ef = ring_comm_ef(options.no_comm_test, options.comm_only_test, options.n, options.send_mode,
        options.recv_mode, rank, size);
    const auto measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, ef, 100);
    const auto stats = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements);
    if (rank == 0)
    {
        const auto send_mode_str = options.send_mode == ring_comm_ef::async ? "async" : "sync";
        const auto recv_mode_str = options.recv_mode == ring_comm_ef::async ? "async" : "sync";
        const auto prefix = std::string("Benchmarking of ring comm with \n");
        tixl::output_results(prefix + send_mode_str + ". sending and " + recv_mode_str + ". receiving.", stats);
    }
    
    // finalize mpi
    MPI_Finalize();
    
    return 0;
}

// END OF FILE
