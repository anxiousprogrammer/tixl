//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a helper function to schedule thread work.

#include <stdexcept> // std::invalid_argument
#include <optional> // std::optional

#include <omp.h>

struct chunkwise_thread_work_plan
{
    std::pair<std::size_t, std::size_t> work_interval = {};
    std::optional<std::size_t> remainder_work_index = {};
};

chunkwise_thread_work_plan schedule_thread_work_chunkwise(const std::size_t& problem_size)
{
    // preconditions
    if (problem_size == 0)
    {
        throw std::invalid_argument("schedule_thread_work_chunkwise_invalid_problem_size_error");
    }
    
    // compute chunk size
    const auto thread_count = omp_get_num_threads();
    const auto thread_id = omp_get_thread_num();
    const auto chunk_size = problem_size / static_cast<std::size_t>(thread_count);
    
    // compute this thread's interval
    const auto work_interval = std::pair<std::size_t, std::size_t>(thread_id * chunk_size, (thread_id + 1) * chunk_size);
    
    // if problem size is not divisible by thread count
    auto remainder_work_index = std::optional<std::size_t>{};
    if (problem_size % thread_count != 0)
    {
        for (auto work_index = chunk_size * thread_count; work_index < problem_size; ++work_index)
        {
            if (thread_id == work_index % thread_count)
            {
                remainder_work_index = work_index;
                break;
            }
        }
    }
    
    return chunkwise_thread_work_plan{work_interval, remainder_work_index};
}
