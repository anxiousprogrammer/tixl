//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a microbenchmark program for the SAXPY/DAXPY loops.

#include "tixl.h"
#include "get_problem_size.h"
#include "schedule_thread_work_chunkwise.h"

#include <cstdlib> // std::rand
#include <utility> // std::abort
#include <functional> // std::reference_wrapper
#include <iostream> // std::cerr
#include <limits> // std::numeric_limits

#include <stdlib.h> // posix_memalign
#include <unistd.h> // sysconf

#include <immintrin.h> // intrinsics

#include <omp.h>

using namespace tixl;
#ifdef DOUBLE_PRECISION
using real_t = double;
#else
using real_t = float;
#endif

//+////////////////////////////////////////////////////////////////////////////////////////////////
// functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace
{

class exp_fn : public experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t problem_size_;
    real_t a_;
    real_t* vec_y_ = nullptr;
    real_t* vec_x_ = nullptr;
    bool use_clflush_ = true;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& problem_size, const real_t& a)
        : problem_size_(problem_size), a_(a)
    {
        if (!__builtin_cpu_supports("sse2"))
        {
            std::cerr << "Warning! The CPU doesn't have the \'sse2\' flag, a feature of which is used to forcefully "
                "evict cache lines to memory." << std::endl;
            use_clflush_ = false;
        }
    }
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate
        const auto page_size = sysconf(_SC_PAGESIZE);
        const auto alloc_status0 = posix_memalign(reinterpret_cast<void**>(&vec_y_), page_size, problem_size_ * sizeof(real_t));
        const auto alloc_status1 = posix_memalign(reinterpret_cast<void**>(&vec_x_), page_size, problem_size_ * sizeof(real_t));
        if (alloc_status0 != 0 || alloc_status1 != 0 || vec_y_ == nullptr || vec_x_ == nullptr)
        {
            std::cerr << "Fatal error, failed to allocate memory." << std::endl;
            std::abort();
        }

        // apply first-touch
        const auto page_size_real = page_size / sizeof(real_t);
        const auto cache_line_size_real = TIXL_CACHE_LINE_SIZE / sizeof(real_t);
        #pragma omp parallel
        {
            const auto thread_work_plan = schedule_thread_work_chunkwise(problem_size_);
            for (auto ii = thread_work_plan.work_interval.first; ii < thread_work_plan.work_interval.second; ii += page_size_real)
            {
                // touch
                constexpr auto modifier_value = std::numeric_limits<real_t>::max();
                vec_y_[ii] = modifier_value;
                vec_x_[ii] = modifier_value;
                
                // flush
                if (use_clflush_)
                {
                    for (auto jj = std::size_t{}; jj < page_size_real; jj += cache_line_size_real)
                    {
                        _mm_clflush(vec_y_ + ii + jj);
                        _mm_clflush(vec_x_ + ii + jj);
                    }
                }
            }
        }
    }
    
    void perform_experiment() final
    {
        #pragma omp parallel
        {
            const auto thread_work_plan = schedule_thread_work_chunkwise(problem_size_);
            #pragma omp simd safelen(16)
            for (auto ii = thread_work_plan.work_interval.first; ii < thread_work_plan.work_interval.second; ++ii)
            {
                vec_y_[ii] += vec_x_[ii] * a_; // fp_count: 2, traffic: 2+1
            }
        }
    }
    
    void finish() final
    {
        std::free(vec_y_);
        std::free(vec_x_);
        vec_y_ = nullptr;
        vec_x_ = nullptr;
    }
};

} // anonymous namespace


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    // calculate the optimal size
    const auto problem_size = get_problem_size<real_t>(argc, argv);
    
    // report number of threads
    const auto problem_size_byte = problem_size * sizeof(real_t);
    std::cout << "Starting runs with " << omp_get_max_threads() << " threads. Problem size is "
        << problem_size_byte << " B (" << static_cast<double>(problem_size_byte) / static_cast<double>(1024 * 1024)
        << " MiB). " << std::endl;
    
    // fix the experiment count
    const auto exp_count = 100;
    
    // perform the schoenauer vector triad experiment
    auto ef2 = exp_fn(problem_size, static_cast<real_t>(0.4));
    const auto measurements = perform_experiments(ef2, exp_count);
    const auto stats = compute_statistics(measurements);
    output_results(
        #ifdef DOUBLE_PRECISION
        "Benchmarking of DAXPY",
        #else
        "Benchmarking of SAXPY",
        #endif
        stats, 2 * problem_size, 3 * problem_size_byte);
    
    return 0;
}

// END OF FILE
