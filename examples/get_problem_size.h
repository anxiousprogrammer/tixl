//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loop
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: n.malapally@fz-juelich.de
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains a helper function to parse problem size as input argument.

#include <stdexcept> // std::invalid_argument

#include <omp.h>

#include <unistd.h> // sysconf

template<class Treal_t>
std::size_t get_problem_size(int argc, char** argv)
{
    if (argc < 2)
    {
        const auto optimal_factor = omp_get_max_threads() * sysconf(_SC_PAGESIZE) / sizeof(Treal_t);
        const auto nonoptimal_problem_size = std::size_t{1024} * 1024 * 1024 / sizeof(Treal_t);
        return nonoptimal_problem_size / optimal_factor * optimal_factor;
    }
    
    const auto problem_size_mib = static_cast<std::size_t>(std::stoul(argv[1]));
    if (problem_size_mib < 1 || problem_size_mib > 100 * 1024)
    {
        throw std::invalid_argument("get_problem_size_invalid_problem_size_error");
    }
    return problem_size_mib * std::size_t{1024} * 1024 / sizeof(Treal_t);
}
