//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'perform_experiments.h'.

#include "perform_experiments.h"
#include "compute_statistics.h"
#include "output_results.h"

#include "test_macros.h"

#include <stdexcept> // std::runtime_error
#include <cmath> // std::fabs
#include <iostream> // std::cout
#include <thread> // std::this_thread::sleep_for
#include <chrono> // std::chrono::milliseconds

using namespace tixl;

//+//////////////////////////////////////////////
// test functor
//+//////////////////////////////////////////////

class exp_fn : public experiment_functor
{

    const std::size_t sleep_duration_;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& sleep_duration)
        : sleep_duration_(sleep_duration) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // nothing to do here
    }
    
    void perform_experiment() final
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(sleep_duration_));
    }
    
    void finish() final
    {
    }
};


//+//////////////////////////////////////////////
// main
//+//////////////////////////////////////////////

int main()
{
    // error
    const auto exp_count = 100;
    const auto max_permissible_error = 200.0;
    
    // perform the test in a loop
    auto max_error = 0.0;
    for (auto duration = 1; duration <= 100; duration *= 10)
    {
        // report
        std::cout << "\nStarting " << duration << " ms test.\n";
        
        // experiments
        auto ef = exp_fn(duration);
        const auto times = perform_experiments(ef, exp_count);
        const auto stats = compute_statistics(times);
        
        // test
        const auto error = std::fabs(static_cast<double>(duration) - stats.am_duration);
        try
        {
            TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
            std::cout << "Accepted an error of " << error << " ms.\n";
        }
        catch(const std::exception&)
        {
            std::cerr << "Test failed! The program will now abort." << std::endl;
            return 1;
        }
        
        // max error
        if (error > max_error)
        {
            max_error = error;
        }
    }
    
    // declare all's well
    std::cout << "\nThe tests have been passed! A maximum error of " << max_error << " ms was recorded." << std::endl;
    
    return 0;
}

// END OF TESTFILE
