//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library TiXL: TImed eXperiment Loops
//    Copyright (C) 2022-2026 Nitin Malapally
//
//    This file is part of the TiXL.
//
//    TiXL is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    TiXL is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test macros for TiXL.

#ifndef TIXL_TEST_H
#define TIXL_TEST_H

#include <stdexcept> // std::runtime_error
#include <string> // std::string
#include <cmath> // std::fabs
#include <iostream> // std::cerr
#include <iomanip> // std::setprecision

#define TIXL_TEST_ASSERT_THROW(X, Y, MSG) \
    { \
        auto test_passed = false; \
        try \
        { \
            X; \
        } catch (const Y& ex) \
        { \
            if (std::string(ex.what()) == std::string(MSG)) \
            { \
                test_passed = true; \
            } \
        } \
        if (!test_passed) \
        { \
            std::cerr << "L" << __LINE__ << ": Throw assert failed!" << std::endl; \
            throw std::runtime_error("throw_assert_failed"); \
        } \
    }

#define TIXL_TEST_ASSERT_ABS_ERROR(ERR, TOL) \
    if (ERR > TOL) \
    { \
        auto default_flags = std::cerr.flags(); \
        std::cerr << std::fixed << std::setprecision(16); \
        std::cerr << "L" << __LINE__ << ": Absolute error too high; value: " << ERR << ", tolerance: " << TOL << "." \
            << std::endl; \
        throw std::runtime_error("abs_error_assert_failed"); \
        std::cerr.flags(default_flags); \
    }

#define TIXL_TEST_ASSERT_EQ(X, Y) \
    if (X != Y) \
    { \
        auto default_flags = std::cerr.flags(); \
        std::cerr << std::fixed << std::setprecision(16); \
        std::cerr << "L" << __LINE__ << ": Equality assert failed; " << X << " != " << Y << std::endl; \
        throw std::runtime_error("equality_assert_failed"); \
        std::cerr.flags(default_flags); \
    }

#define TIXL_TEST_FP_ASSERT_EQ(X, Y, TOL) \
    if (std::fabs(X - Y) > TOL) \
    { \
        auto default_flags = std::cerr.flags(); \
        std::cerr << std::fixed << std::setprecision(16); \
        std::cerr << "L" << __LINE__ << ": Floating-point equality assert failed; " << X << " != " << Y << std::endl; \
        throw std::runtime_error("fp_equality_assert_failed"); \
        std::cerr.flags(default_flags); \
    }

#endif // TIXL_TEST_H
