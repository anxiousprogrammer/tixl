//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'spot_profiler.h'.

#include "spot_profiler.h"
#include "compute_statistics.h"
#include "output_results.h"

#include "test_macros.h"

#include <string> // std::string
#include <iostream> // std::cerr
#include <stdexcept> // std::runtime_error
#include <thread> // std::this_thread::sleep_for
#include <chrono> // std::chrono::milliseconds
#include <cmath> // std::fabs
#include <algorithm> // std::max

using namespace tixl;

//+//////////////////////////////////////////////
// main
//+//////////////////////////////////////////////

int main()
{
    auto max_error = 0.0;
    try
    {
        // error case: try to open a spot without opening a window
        TIXL_TEST_ASSERT_THROW(spot_profiler::get().open_spot("ERROR"), std::runtime_error,
            "tixl_spot_profiler_impl_open_spot_no_open_window_error");
        
        // error case: try to close a spot without opening a window
        TIXL_TEST_ASSERT_THROW(spot_profiler::get().close_spot("ERROR"), std::runtime_error,
            "tixl_spot_profiler_impl_close_spot_no_open_window_error");
        
        // error case: try to close a window without opening it
        TIXL_TEST_ASSERT_THROW(spot_profiler::get().close_window(), std::runtime_error,
            "tixl_spot_profiler_impl_close_window_not_yet_opened_error");
        
        // open a window
        spot_profiler::get().open_window();
        
        // error case: try to open a spot with empty key
        TIXL_TEST_ASSERT_THROW(spot_profiler::get().open_spot(""), std::runtime_error,
            "tixl_spot_profiler_impl_open_spot_empty_key_error");
        
        // error case: try to open a spot with too long a key
        {
            const auto too_long_key = std::string(
                "tooooooooooooooooooooooooooooooooooooooooooooooooollllllllooooooooooooooooooooooooooooooooooooooonngg");
            TIXL_TEST_ASSERT_THROW(spot_profiler::get().open_spot(too_long_key), std::runtime_error,
                "tixl_spot_profiler_impl_open_spot_key_too_long_error");
        }
        
        // error case: try to close a spot without opening it
        TIXL_TEST_ASSERT_THROW(spot_profiler::get().close_spot("S0"), std::runtime_error,
            "tixl_spot_profiler_impl_open_spot_unable_to_find_spot_error");
        
        //+/////////////////
        // 10 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 50; ++ii)
        {
            spot_profiler::get().open_spot("10 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            spot_profiler::get().close_spot("10 ms");
        }
        
        //+/////////////////
        // 10 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 20; ++ii)
        {
            spot_profiler::get().open_spot("50 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            spot_profiler::get().close_spot("50 ms");
        }
        
        //+/////////////////
        // 100 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 10; ++ii)
        {
            spot_profiler::get().open_spot("100 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            spot_profiler::get().close_spot("100 ms");
        }
        
        //+/////////////////
        // 1000 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 3; ++ii)
        {
            spot_profiler::get().open_spot("1000 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            spot_profiler::get().close_spot("1000 ms");
        }
        
        // close the window
        spot_profiler::get().close_window();
        
        // error case: try to close the window without re-opening it
        TIXL_TEST_ASSERT_THROW(spot_profiler::get().close_window(), std::runtime_error,
            "tixl_spot_profiler_impl_close_window_not_yet_opened_error");
        
        // test for error -> 10 ms test
        const auto max_permissible_error = 200.0;
        {
            const auto key = "10 ms";
            const auto stats_element = spot_profiler::get().get_stats(key);
            const auto error = std::fabs(10.0 - stats_element.am_duration);
            TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
            std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\".\n";
            max_error = std::max(max_error, error);
        }
        
        // test for error -> 50 ms test
        {
            const auto key = "50 ms";
            const auto stats_element = spot_profiler::get().get_stats(key);
            const auto error = std::fabs(50.0 - stats_element.am_duration);
            TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
            std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\".\n";
            max_error = std::max(max_error, error);
        }
        
        // test for error -> 100 ms test
        {
            const auto key = "100 ms";
            const auto stats_element = spot_profiler::get().get_stats(key);
            const auto error = std::fabs(100.0 - stats_element.am_duration);
            TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
            std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\".\n";
            max_error = std::max(max_error, error);
        }
        
        // test for error -> 1000 ms test
        {
            const auto key = "1000 ms";
            const auto stats_element = spot_profiler::get().get_stats(key);
            const auto error = std::fabs(1000.0 - stats_element.am_duration);
            TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
            std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\".\n";
            max_error = std::max(max_error, error);
        }
        
        // report
        spot_profiler::get().output_results("spot_profiler Test");
        
        // error case: try to reopen the window
        TIXL_TEST_ASSERT_THROW(spot_profiler::get().open_window(), std::runtime_error,
            "tixl_spot_profiler_impl_open_window_reopening_not_possible_error");
    }
    catch (const std::exception&)
    {
        std::cerr << "Test Failed! The program will now abort." << std::endl;
        return 1;
    }
    
    // declare all's well
    std::cout << "\nThe tests have been passed! A maximum error of " << max_error << " ms was recorded." << std::endl;
    
    return 0;
}

// END OF TESTFILE
