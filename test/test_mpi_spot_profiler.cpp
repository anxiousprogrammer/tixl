//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'mpi_spot_profiler.h'.

#include "mpi_spot_profiler.h"
#include "mpi_compute_statistics.h"
#include "output_results.h"

#include "test_macros.h"

#include <string> // std::string
#include <iostream> // std::cerr
#include <stdexcept> // std::runtime_error
#include <thread> // std::this_thread::sleep_for
#include <chrono> // std::chrono::milliseconds
#include <cmath> // std::fabs
#include <algorithm> // std::max

#include <mpi.h>

using namespace tixl;

//+//////////////////////////////////////////////
// main
//+//////////////////////////////////////////////

int main(int argc, char** argv)
{
    //+//////////////////////////////////////////
    // MPI init
    //+//////////////////////////////////////////

    int provided = 0;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cout << "The MPI implementation does not support multi-threaded message passing." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    // check conditions
    const auto min_size = 6;
    if (world_rank == 0)
    {
        if (world_size < min_size)
        {
            std::cout << "Please run the test using at least " << min_size << " processes" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
        }
    }
    
    auto max_error = 0.0;
    try
    {
        // error case: try to open a spot without opening a window
        TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().open_spot("ERROR"), std::runtime_error,
            "tixl_spot_profiler_impl_open_spot_no_open_window_error");
        
        // error case: try to close a spot without opening a window
        TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().close_spot("ERROR"), std::runtime_error,
            "tixl_spot_profiler_impl_close_spot_no_open_window_error");
        
        // error case: try to close a window without opening it
        TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().close_window(MPI_COMM_WORLD), std::runtime_error,
            "tixl_spot_profiler_impl_close_window_not_yet_opened_error");
        
        // open a window
        mpi_spot_profiler::get().open_window();
        
        // error case: try to open a spot with empty key
        TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().open_spot(""), std::runtime_error,
            "tixl_spot_profiler_impl_open_spot_empty_key_error");
        
        // error case: try to open a spot with too long a key
        {
            const auto too_long_key = std::string(
                "tooooooooooooooooooooooooooooooooooooooooooooooooollllllllooooooooooooooooooooooooooooooooooooooonngg");
            TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().open_spot(too_long_key), std::runtime_error,
                "tixl_spot_profiler_impl_open_spot_key_too_long_error");
        }
        
        // error case: try to close a spot without opening it
        TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().close_spot("S0"), std::runtime_error,
            "tixl_spot_profiler_impl_open_spot_unable_to_find_spot_error");
        
        //+/////////////////
        // 10 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 50; ++ii)
        {
            mpi_spot_profiler::get().open_spot("10 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            mpi_spot_profiler::get().close_spot("10 ms");
        }
        
        //+/////////////////
        // 10 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 20; ++ii)
        {
            mpi_spot_profiler::get().open_spot("50 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            mpi_spot_profiler::get().close_spot("50 ms");
        }
        
        //+/////////////////
        // 100 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 10; ++ii)
        {
            mpi_spot_profiler::get().open_spot("100 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            mpi_spot_profiler::get().close_spot("100 ms");
        }
        
        //+/////////////////
        // 1000 ms test
        //+/////////////////
        
        for (auto ii = std::size_t{}; ii < 3; ++ii)
        {
            mpi_spot_profiler::get().open_spot("1000 ms");
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            mpi_spot_profiler::get().close_spot("1000 ms");
        }
        
        //+/////////////////
        // test: not all ranks are active
        //+/////////////////
        
        if (world_rank == 2 || world_rank == 3)
        {
            for (auto ii = std::size_t{}; ii < 20; ++ii)
            {
                mpi_spot_profiler::get().open_spot("test::not_all_ranks_are_active");
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
                mpi_spot_profiler::get().close_spot("test::not_all_ranks_are_active");
            }
        }
        mpi_spot_profiler::get().declare_unreachable_spot("test::not_all_ranks_are_active");
        
        // close the window
        mpi_spot_profiler::get().close_window(MPI_COMM_WORLD);
        
        // error case: try to close the window without re-opening it
        TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().close_window(MPI_COMM_WORLD), std::runtime_error,
            "tixl_spot_profiler_impl_close_window_not_yet_opened_error");
        
        // test
        if (world_rank == 0)
        {
            const auto max_permissible_error = 0.5;
            
            // test for error -> 10 ms test
            {
                const auto ref_time = 10.0;
                const auto key = std::to_string(static_cast<std::uint64_t>(ref_time)) + " ms";
                for (auto ii = unsigned{}; ii < world_size; ++ii)
                {
                    const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_stats(key, ii).am_duration);
                    TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                    std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for rank " << ii << ".\n";
                    max_error = std::max(max_error, error);
                }
                const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_all_ranks_stats(key).am_duration);
                TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for all ranks.\n";
            }
            
            // test for error -> 50 ms test
            {
                const auto ref_time = 50.0;
                const auto key = std::to_string(static_cast<std::uint64_t>(ref_time)) + " ms";
                for (auto ii = unsigned{}; ii < world_size; ++ii)
                {
                    const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_stats(key, ii).am_duration);
                    TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                    std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for rank " << ii << ".\n";
                    max_error = std::max(max_error, error);
                }
                const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_all_ranks_stats(key).am_duration);
                TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for all ranks.\n";
            }
            
            // test for error -> 100 ms test
            {
                const auto ref_time = 100.0;
                const auto key = std::to_string(static_cast<std::uint64_t>(ref_time)) + " ms";
                for (auto ii = unsigned{}; ii < world_size; ++ii)
                {
                    const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_stats(key, ii).am_duration);
                    TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                    std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for rank " << ii << ".\n";
                    max_error = std::max(max_error, error);
                }
                const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_all_ranks_stats(key).am_duration);
                TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for all ranks.\n";
            }
            
            // test for error -> 1000 ms test
            {
                const auto ref_time = 1000.0;
                const auto key = std::to_string(static_cast<std::uint64_t>(ref_time)) + " ms";
                for (auto ii = unsigned{}; ii < world_size; ++ii)
                {
                    const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_stats(key, ii).am_duration);
                    TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                    std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for rank " << ii << ".\n";
                    max_error = std::max(max_error, error);
                }
                const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_all_ranks_stats(key).am_duration);
                TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for all ranks.\n";
            }
            
            // test for error -> "not all ranks are active" test
            {
                const auto ref_time = 50.0;
                const auto key = "test::not_all_ranks_are_active";
                for (auto ii = unsigned{}; ii < world_size; ++ii)
                {
                    if (world_rank == 2 || world_rank == 3)
                    {
                        const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_stats(key, ii).am_duration);
                        TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                        std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for rank " << ii << ".\n";
                        max_error = std::max(max_error, error);
                    }
                }
                const auto error = std::fabs(ref_time - mpi_spot_profiler::get().get_all_ranks_stats(key).am_duration);
                TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                std::cout << "Accepted an error of " << error << " ms for spot \"" << key << "\" for all ranks.\n";
            }
            
            // report
            if (world_rank == 0)
            {
                mpi_spot_profiler::get().output_results("mpi_spot_profiler Test");
            }
        }
        
        // error case: try to reopen the window
        TIXL_TEST_ASSERT_THROW(mpi_spot_profiler::get().open_window(), std::runtime_error,
            "tixl_spot_profiler_impl_open_window_reopening_not_possible_error");
    }
    catch (const std::exception& ex)
    {
        std::cerr << "Test Failed! The program will now abort. What: \"" << ex.what() << "\"" << std::endl;
        return 1;
    }
    
    // declare all's well
    if (world_rank == 0)
    {
        std::cout << "\nThe tests have been passed! A maximum error of " << max_error << " ms was recorded." << std::endl;
    }
    
    // don't forget to finalize!
    MPI_Finalize();
    
    return 0;
}

// END OF TESTFILE
