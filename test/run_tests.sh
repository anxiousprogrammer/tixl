#!/bin/bash
printf "\n---------------------------------------\nTEST: compute statistics\n"
./test_compute_statistics

printf "\n---------------------------------------\nTEST: perform experiments\n"
./test_perform_experiments

printf "\n---------------------------------------\nTEST: MPI perform experiments\n"
mpirun -n 2 ./test_mpi_perform_experiments

printf "\n---------------------------------------\nTEST: spot profiler\n"
./test_spot_profiler

printf "\n---------------------------------------\nTEST: MPI spot profiler\n"
mpirun -n 6 ./test_mpi_spot_profiler
