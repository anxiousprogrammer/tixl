//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'compute_statistics.h'.

#include "compute_statistics.h"

#include "test_macros.h"

#include <exception> // std::exception
#include <vector> // std::vector
#include <iostream> // std::cerr
#include <cstdint> // std::uint64_t

using namespace tixl;

//+//////////////////////////////////////////////
// test
//+//////////////////////////////////////////////

void perform_test()
{
    auto times = std::vector<std::uint64_t>{1000, 3000, 2300, 1200, 10000, 3330, 2800, 5000};
    const auto results = compute_statistics(times);
    
    TIXL_TEST_ASSERT_EQ(times.size(), results.exp_count);
    
    const auto max_permissible_error = 1E-12;
    TIXL_TEST_FP_ASSERT_EQ(3.5787500000000000, results.am_duration, max_permissible_error);
    TIXL_TEST_FP_ASSERT_EQ(2.8075359481462745, results.gm_duration, max_permissible_error);
    
    TIXL_TEST_FP_ASSERT_EQ(1.0, results.min_duration, max_permissible_error);
    TIXL_TEST_FP_ASSERT_EQ(10.0, results.max_duration, max_permissible_error);
    
    TIXL_TEST_FP_ASSERT_EQ(2.6972042817517550, results.std_dev_duration, max_permissible_error);
    TIXL_TEST_FP_ASSERT_EQ(0.2913563849938242, results.std_dev_inv_duration, max_permissible_error);
}


//+//////////////////////////////////////////////
// main
//+//////////////////////////////////////////////

int main()
{
    try
    {
        perform_test();
    }
    catch(const std::exception&)
    {
        std::cerr << "Test failed! The program will now abort." << std::endl;
        return 1;
    }
    
    // declare all's well
    std::cout << "The tests have been passed!" << std::endl;
    
    return 0;
}

// END OF TESTFILE
