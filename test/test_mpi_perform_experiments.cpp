//+////////////////////////////////////////////////////////////////////////////////////////////////
//    Library S3DFT: Scalable 3D-DFT
//    Copyright (C) 2021-2025 Nitin Malapally
//
//    This file is part of the S3DFT.
//
//    S3DFT is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as
//    published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    S3DFT is distributed in the hope that it will be useful, but
//    WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

/// \file This file contains test(s) for 'mpi_perform_experiments.h'.

#include "mpi_perform_experiments.h"
#include "mpi_compute_statistics.h"
#include "output_results.h"

#include "test_macros.h"

#include <stdexcept> // std::runtime_error
#include <cmath> // std::fabs
#include <iostream> // std::cerr
#include <thread> // std::this_thread::sleep_for
#include <chrono> // std::chrono::milliseconds

#include <mpi.h>


using namespace tixl;

//+//////////////////////////////////////////////
// test functor
//+//////////////////////////////////////////////

class exp_fn : public experiment_functor
{

    const std::size_t sleep_duration_;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& sleep_duration)
        : sleep_duration_(sleep_duration) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // nothing to do here
    }
    
    void perform_experiment() final
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(sleep_duration_));
    }
    
    void finish() final
    {
    }
};


//+//////////////////////////////////////////////
// main
//+//////////////////////////////////////////////

int main(int argc, char** argv)
{
    //+//////////////////////////////////////////
    // MPI init
    //+//////////////////////////////////////////

    int provided = 0;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cout << "The MPI implementation does not support multi-threaded message passing." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
    }
    
    // obtain rank and size
    auto world_rank = std::numeric_limits<int>::max();
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    
    // check conditions
    const auto min_size = 2;
    if (world_rank == 0)
    {
        if (world_size < min_size)
        {
            std::cout << "Please run the test using at least " << min_size << " processes" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_SIZE);
        }
    }
    
    //+//////////////////////////////////////////
    // testing
    //+//////////////////////////////////////////
    
    // error
    const auto exp_count = 100;
    const auto max_permissible_error = 300.0;
    
    // perform the test in a loop
    auto max_error = 0.0;
    for (auto duration = 1; duration <= 100; duration *= 10)
    {
        // report
        if (world_rank == 0)
        {
            std::cout << "\nStarting " << duration << " ms test.\n";
        }
        
        // experiments
        auto ef = exp_fn(duration);
        const auto times = mpi_perform_experiments(MPI_COMM_WORLD, ef, exp_count);
        const auto stats = mpi_compute_statistics(MPI_COMM_WORLD, times);
        
        // test
        auto error = 0.0;
        try
        {
            if (world_rank == 0)
            {
                error = std::fabs(static_cast<double>(duration) - stats.am_duration);
                TIXL_TEST_ASSERT_ABS_ERROR(error, max_permissible_error);
                std::cout << "Accepted an error of " << error << " ms.\n";
            }
            else
            {
                TIXL_TEST_ASSERT_EQ(stats.am_duration, 0.0);
            }
        }
        catch(const std::exception&)
        {
            if (world_rank == 0)
            {
                std::cerr << "Test failed! The program will now abort." << std::endl;
            }
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        
        // max error
        if (error > max_error)
        {
            max_error = error;
        }
    }
    
    // declare all's well
    if (world_rank == 0)
    {
        std::cout << "\nThe tests have been passed! A maximum error of " << max_error << " ms was recorded." << std::endl;
    }
    
    // don't forget to finalize!
    MPI_Finalize();
    
    return 0;
}

// END OF TESTFILE
